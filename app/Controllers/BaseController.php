<?php namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use App\Models\Admin\LogoModel;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use DateTime;
use Psr\Log\LoggerInterface;

class BaseController extends Controller
{
    public $hasSupportingFiles = true;
    public $module_title;
    public $submodule_title;
    public $controller_path;
    public $controller_path_url;

    public $model;
    public $view_data;

    /**
     * An array of helpers to be loaded automatically upon
     * class instantiation. These helpers will be available
     * to all other controllers that extend BaseController.
     *
     * @var array
     */
    protected $helpers = ['access_control', 'file', 'string_helper',
        'output', 'tables_link', 'uri', 'array_helper'
    ];

    /**
     * Constructor.
     */
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        //--------------------------------------------------------------------
        // Preload any models, libraries, etc, here.
        //--------------------------------------------------------------------
        // E.g.:
        // $this->session = \Config\Services::session();

        if (!empty($this->request->uri->getTotalSegments() >= 3)) {
            if ($this->request->uri->getSegment(3) != "html") {
                if (!user_access_check()) {
                    exit();
                }
            }
        }
    }

    public function process_add()
    {
        $errors = array();      // array to hold validation errors
        $data = array(
            "approval_status" => "Approved"
        );

        foreach ($_POST as $key => $value) {
            $data[$key] = $value;
        }
        unset($data["btn_submit"]);

        $items = array();
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                if (array_depth($value) == 1) {
                    $array = $value;
                    $modified = implode("##", $array);
                    $_POST[$key] = $modified;
                } else if (array_depth($value) == 2) {
                    $items[$key] = $value;
                    unset($data[$key]);
                }
            }
        }

        $files = $_FILES;
        unset($files["file_upload"]);
        foreach ($files as $key => $value) {
            $file_upload = $this->upload_file($key);

            if ($file_upload == null){
                continue;
            }

            if ($file_upload["status"] == "success") {
                $file_url = base_url("uploads/" . $file_upload["filename"]);
            } else if ($file_upload["status"] == "failed") {
                return false;
            }
            $data[$key] = empty($file_url) ? "" : $file_url;
        }

        // if there are any errors in our errors array, return a success boolean of false
        if (!empty($errors)) {

            // if there are items in our errors array, return those errors
            $response_data["success"] = false;
            $response_data["errors"] = $errors;

        } else {

            if (!empty($_FILES["file_upload"]["name"])) {
                $file_upload = $this->upload_file("file_upload");

                if ($file_upload["status"] == "success") {
                    $file_url = base_url("uploads/" . $file_upload["filename"]);
                } else if ($file_upload["status"] == "failed") {
                    return false;
                }
                $data["file_url"] = empty($file_url) ? "" : $file_url;
            }

            // if there are no errors process our form, then return a message
            $isSuccess = $this->model->insert($data);

            if (!empty($items)) {
                foreach ($items as $key => $value) {
                    if (is_array($value)) {
                        $table_name = $key;
                        $array = $value;
                        foreach ($array as $dataObject) {
                            $insertedID = $isSuccess;
                            $dataObject["ref_id"] = $insertedID;
                            $this->model->insertItems($dataObject, $table_name);
                        }
                    }
                }
            }

            // DO ALL YOUR FORM PROCESSING HERE
            // show a message of success and provide a true success variable
            if ($isSuccess) {
                $response_data["success"] = true;
                $response_data["message"] = "Add data succeed" . (isset($file_upload) ? ", " . $file_upload["message"] : "");
                header("refresh:2; url=" . base_url($this->controller_path . "/list"));
            } else {
                $response_data["success"] = false;
                $response_data["errors"] = $errors;
                $response_data["message"] = "Add data failed" . (isset($file_upload) ? ", " . $file_upload["message"] : "");
                echo "<script>setTimeout(function() {history.back()}, 2000)</script>";
            }
        }

        echo $response_data["message"];
    }

    protected function upload_file($file_input_name = NULL)
    {
        if ($file_input_name == NULL) {
            $file_input_name = "file_upload";
        }

        $date = DateTime::createFromFormat('U.u', microtime(TRUE));
        $file_name = $date->format('Y_m_d_H_i_s_u');

        $mime_validation = $this->validate([
            $file_input_name => [
                "uploaded[$file_input_name]",
                "mime_in[$file_input_name,image/jpg,image/jpeg,image/gif,image/png,application/pdf,text/plain,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword]"
            ],
        ]);

        $size_validation = $this->validate([
            $file_input_name => [
                "uploaded[$file_input_name]",
                "max_size[$file_input_name,4096]"
            ],
        ]);
        // $config['allowed_types'] = "gif|jpg|jpeg|png|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|txt";

        $file_upload = null;

        if ($mime_validation && $size_validation) {
            $file_upload = $this->request->getFile($file_input_name);

            $file_name = $file_name . "_" . $file_upload->getClientName();
            $file_upload->move(ROOTPATH . "public/uploads", $file_name);
        }

        $output = array(
            "status" => "",
            "filename" => "",
            "message" => ""
        );

        if ($file_upload == null){
            return null;
        }

        if ($file_upload->hasMoved()) {
            $output['status'] = "success";
            $output['message'] = "File upload success";
            $output['filename'] = $file_name;
        } else {
            $output['status'] = "failed";

            if (!$mime_validation) {
                $output['message'] = "Your file type is not valid";
            } else if (!$size_validation) {
                $output['message'] = "File upload mustbe under 1MB (max 1024KB)";
            } else {
                $output['message'] = "File upload failed";
            }
        }

        return $output;
    }

    public function process_edit()
    {
        $data = array();

        foreach ($_POST as $key => $value) {
            $data[$key] = $value;
        }
        unset($data["id"]);
        unset($data["btn_submit"]);

        $items = array();
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                if (array_depth($value) == 1) {
                    $array = $value;
                    $modified = implode("##", $array);
                    $_POST[$key] = $modified;
                } else if (array_depth($value) == 2) {
                    $items[$key] = $value;
                    unset($data[$key]);
                }
            }
        }

        if (!empty($_FILES["file_upload"]["name"])) {
            $row = $this->model->find($_POST["id"]);

            $file_upload = $this->upload_file("file_upload");

            if ($file_upload["status"] == "success") {
                $file_url = base_url("uploads/" . $file_upload["filename"]);
                if (!empty($row->file_url)) {
                    delete_file_url($row->file_url);
                }
            } else if ($file_upload["status"] == "failed") {
                return false;
            }
            $data["file_url"] = empty($file_url) ? "" : $file_url;
        }

        $isSuccess = $this->model->update($_POST["id"], $data);

        if (!empty($items)) {
            foreach ($items as $key => $value) {
                if (is_array($value)) {
                    $table_name = $key;
                    $array = $value;

                    $this->model->deleteItems($_POST["id"], $table_name);
                    foreach ($array as $dataObject) {
                        $dataObject["ref_id"] = $_POST["id"];
                        $this->model->insertItems($dataObject, $table_name);
                    }
                }
            }
        }

        if ($isSuccess) {
            $response_data["success"] = true;
            $response_data["message"] = "Update data succeed" . (isset($file_upload) ? ", " . $file_upload["message"] : "");
            header("refresh:2; url=" . base_url($this->controller_path . "/edit/" . $_POST["id"]));
        } else {
            $response_data["success"] = false;
            $response_data["message"] = "Update data failed" . (isset($file_upload) ? ", " . $file_upload["message"] : "");
            echo "<script>setTimeout(function() {history.back()}, 2000)</script>";
        }

        echo $response_data["message"];
    }

    public function process_delete($id)
    {
        $row = $this->model->find($id);
        if (!empty($row->file_url)) {
            delete_file_url($row->file_url);
        }

        if ($this->model->delete($id)) {
            $response_data["success"] = true;
            $response_data["message"] = "Delete data succeed " . (!empty($row->file_url) ? $row->file_url : "");
        } else {
            $response_data["success"] = false;
            $response_data["message"] = "Delete data failed ";
        }
        header("refresh:2; url=" . base_url($this->controller_path . "/list"));

        echo $response_data["message"];
    }

    public function html($id)
    {
        $this->LogoModel = new LogoModel();
        $logo = (new LogoModel())->where('active', 1)->first();
        $this->view_data["logo"] = $logo;

        $row = $this->model->find($id);
        foreach ($row as $key => $value) {
            $this->view_data[$key] = $value;
        }

        echo view("{$this->controller_path}_pdf", $this->view_data);
    }

    public function distribution($id)
    {
        $html_url = base_url("{$this->controller_path}/html/$id");
        //$htmlToPdf = new HtmlToPdf();
        //$htmlToPdf->display_pdf($html_url);
        header("refresh:0; url=http://asiaglobalservis.id/urltopdf.php?url=$html_url");
    }

}
