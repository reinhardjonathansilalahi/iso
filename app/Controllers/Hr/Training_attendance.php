<?php namespace App\Controllers\Hr;

use App\Controllers\BaseController;

class Training_Attendance extends BaseController
{

    private $module_title;
    private $submodule_title;
    private $controller_path;
    private $controller_path_url;

	public function __construct(){
		parent::__construct();

        if ($this->request->uri->getSegment(3) != "html") {
            if (!user_access_check()) {
                exit();
            }
        }

        $this->load->model("HR/TrainingAttendanceModel");
        $this->load->model("HR/DepartmentModel");

        $this->module_title = "HR";
        $this->submodule_title = "Training Attendance";
        $this->controller_path = "hr/training_attendance";
        $this->controller_path_url = base_url($this->controller_path);
    }

    public function list()
    {
        $data = array(
            "title"			=> "Training Attendance",
            "rows" 			=> $this->TrainingAttendanceModel->read()->result(),
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path_url" => $this->controller_path_url,
        );
        echo view("hr/training_attendance_read", $data);
    }

    public function create()
    {
        $data = array(
            "title"			=> "Create New Training Attendance",
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path_url" => $this->controller_path_url,
        );

        echo view("hr/training_attendance_create", $data);
    }

    public function edit($id)
    {
        $data = array(
            "title"			=> "Edit Training Attendance",
            "row"			=> $this->TrainingAttendanceModel->readItem($id)->row(),
            "rows"  		=> $this->TrainingAttendanceModel->readItems($id)->result(),
            "rows2"  		=> $this->TrainingAttendanceModel->readItems2($id)->result(),
            "id" 			=> $id,
            "departments" => $this->DepartmentModel->read()->result(),
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path_url" => $this->controller_path_url,
        );

        echo view("hr/training_attendance_edit", $data);
    }

    public function process_edit()
    {
        if (empty($_POST["datas2"])) {
            $errors["datas2"] = <<<EOT
			Toast.fire({
				type: "error",
				title: "Data Evaluator tidak boleh kosong, silakan tambah data evaluator"
			});
EOT;
        }

        // return a response ===========================================================

        // if there are any errors in our errors array, return a success boolean of false
        if (!empty($errors)) {

            // if there are items in our errors array, return those errors
            $response_data["success"] = false;
            $response_data["errors"]  = $errors;
        } else {
            $data		= array(
                "id"					=>  $_POST["id"],
                "datas"					=> 	$_POST["datas"],
                "datas2"				=> 	$_POST["datas2"],
                "data"					=>  $_POST["data"]
            );

            if ($this->TrainingAttendanceModel->edit($data)) {
                $response_data["success"] = true;
                $response_data["message"] = "Ubah data berhasil";
                header("refresh:2; url=".base_url($this->controller_path . "/edit/" . $_POST['id']));
            } else {
                $response_data["success"] = false;
                $response_data["message"] = "Ubah data gagal";
                echo "<script>setTimeout(function() {history.back()}, 2000)</script>";
            }
        }


        echo json_encode($response_data);
    }

    public function process_delete($id)
    {

        if ($this->TrainingAttendanceModel->delete($id)) {
            $response_data["success"] = true;
            $response_data["message"] = "Hapus data berhasil";
            header("refresh:2; url=".base_url($this->controller_path . "/list"));
        } else {
            $response_data["success"] = false;
            $response_data["message"] = "Hapus data gagal";
            header("refresh:2; url=".base_url($this->controller_path . "/list"));
        }

        echo $response_data["message"];
    }
}
