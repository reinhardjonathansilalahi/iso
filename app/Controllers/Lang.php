<?php namespace App\Controllers;

use Config\Services;

class Lang extends BaseController
{
    function switchLanguage($language = "") {
        $language = ($language != "") ? $language : "english";
        Services::session()->set("site_lang", $language);
        redirect()->to(base_url());
    }
}
