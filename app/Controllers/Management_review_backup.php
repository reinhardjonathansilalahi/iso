<?php namespace App\Controllers;

class Management_reviewBackup extends BaseController
{
    private $module_title;
    private $submodule_title;
    private $controller_path;
    private $controller_path_url;
    private $step_type = 7;
    private $HtmlToPDFLibrary;

    public function __construct()
    {
        parent::__construct();

        if ($this->request->uri->getSegment(3) != "html") {
            if (!user_access_check()) {
                exit();
            }
        }

        $this->load->library("Tracking");
        $this->load->library("HtmlToPdf");
        $this->load->model("ManagementReviewModel", "model");
        $this->load->model("Admin/LogoModel");

        $this->controller_path = "management_review";
        $this->controller_path_url = base_url($this->controller_path);

        $this->module_title = "Management Review";
        $this->submodule_title = " . ";
    }

    public function html($id)
    {
        $row = $this->model->readItem($id);
        $logo = $this->LogoModel->logoActive();

        $data = array(
            "logo" => $logo,
            "module_title" => $this->module_title,
            "hari_tanggal" => $_POST["hari_tanggal"],
            "waktu" => $_POST["waktu"],
            "tempat" => $_POST["tempat"],
            "topik_sebelum" => $_POST["topik_sebelum"],
            "uraian_sebelum" => $_POST["uraian_sebelum"],
            "pic_sebelum" => $_POST["pic_sebelum"],
            "batas_waktu_sebelum" => $_POST["batas_waktu_sebelum"],
            "status_sebelum" => $_POST["status_sebelum"],
            "keterangan_sebelum" => $_POST["keterangan_sebelum"],
            "topik_sesudah" => $_POST["topik_sesudah"],
            "uraian_sesudah" => $_POST["uraian_sesudah"],
            "pic_sesudah" => $_POST["pic_sesudah"],
            "batas_waktu_sesudah" => $_POST["batas_waktu_sesudah"],
            "status_sesudah" => $_POST["status_sesudah"],
            "keterangan_sesudah" => $_POST["keterangan_sesudah"],
            "approval_status" => "Menunggu persetujuan MGR",
            "approval_1" => get_approval_val($row->approval_1),
            "approval_2" => get_approval_val($row->approval_2),
            "ttd1" => get_ttd_val($row->approval_1),
            "ttd2" => get_ttd_val($row->approval_2)
        );

        echo view($this->controller_path . "/pdf", $data);
    }

    public function distribution($id)
    {
        $html_url = base_url("{$this->controller_path}/html/$id");
        $this->htmltopdf->display_pdf($html_url);
    }

    public function list()
    {
        $data = array(
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path_url" => $this->controller_path_url,
            "rows" => $this->model
                ->read(session()->get("id"))
                ->result()
        );
        echo view($this->controller_path . "/read", $data);
    }

    public function create()
    {
        $data = array(
            "module_title" => "Create New " . $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path_url" => $this->controller_path_url,
        );
        echo view($this->controller_path . "/create", $data);
    }

    public function edit($id)
    {
        $data = array(
            "module_title" => "Edit " . $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path_url" => $this->controller_path_url,
            "row" => $this->model->readItem($id)
        );

        echo view($this->controller_path . "/edit", $data);
    }

    public function process_add()
    {
        $errors = array(); // array to hold validation errors
        $data = array(); // array to pass back data

        $data_pass = array(
            "hari_tanggal" => $_POST["hari_tanggal"],
            "waktu" => $_POST["waktu"],
            "tempat" => $_POST["tempat"],
            "topik_sebelum" => $_POST["topik_sebelum"],
            "uraian_sebelum" => $_POST["uraian_sebelum"],
            "pic_sebelum" => $_POST["pic_sebelum"],
            "batas_waktu_sebelum" => $_POST["batas_waktu_sebelum"],
            "status_sebelum" => $_POST["status_sebelum"],
            "keterangan_sebelum" => $_POST["keterangan_sebelum"],
            "topik_sesudah" => $_POST["topik_sesudah"],
            "uraian_sesudah" => $_POST["uraian_sesudah"],
            "pic_sesudah" => $_POST["pic_sesudah"],
            "batas_waktu_sesudah" => $_POST["batas_waktu_sesudah"],
            "status_sesudah" => $_POST["status_sesudah"],
            "keterangan_sesudah" => $_POST["keterangan_sesudah"],
            "step" => "1",
            "approval_status" => "Menunggu persetujuan MGR",
        );

        // if there are any errors in our errors array, return a success boolean of false
        if (!empty($errors)) {
            // if there are items in our errors array, return those errors
            $response_data["success"] = false;
            $response_data["errors"] = $errors;
        } else {
            // if there are no errors process our form, then return a message

            $isSuccess = $this->model->insert($data_pass);

            $mgr = $this->tracking->mgr(session()->get("id"));
            $vp = $this->tracking->vp(session()->get("id"));
            $mr = $this->tracking->mr(session()->get("id"));

            $this->model->insertTracking(
                $this->tracking->insertTracking($isSuccess, $this->step_type, 0, session()->get("id"))
            );
            $this->model->insertTracking(
                $this->tracking->insertTracking($isSuccess, $this->step_type, 1, $mgr)
            );
            $this->model->insertTracking(
                $this->tracking->insertTracking($isSuccess, $this->step_type, 2, $vp)
            );
            $this->model->insertTracking(
                $this->tracking->insertTracking($isSuccess, $this->step_type, 3, $mr)
            );

            // show a message of success and provide a true success variable
            if ($isSuccess) {
                 header("refresh:2; url=". base_url() . "/" . $this->controller_path . "/" . "list");
            } else {
                 header("refresh:2; url=". base_url() . "/" . $this->controller_path . "/" . "create");
            }
        }

        echo $response_data["message"];
    }

    public function process_edit()
    {
        if ($_POST["btn_submit"] == "edit") {
            $data = array(
                "id" => $_POST["id"],
                "hari_tanggal" => $_POST["hari_tanggal"],
                "waktu" => $_POST["waktu"],
                "tempat" => $_POST["tempat"],
                "topik_sebelum" => $_POST["topik_sebelum"],
                "uraian_sebelum" => $_POST["uraian_sebelum"],
                "pic_sebelum" => $_POST["pic_sebelum"],
                "batas_waktu_sebelum" => $_POST["batas_waktu_sebelum"],
                "status_sebelum" => $_POST["status_sebelum"],
                "keterangan_sebelum" => $_POST["keterangan_sebelum"],
                "topik_sesudah" => $_POST["topik_sesudah"],
                "uraian_sesudah" => $_POST["uraian_sesudah"],
                "pic_sesudah" => $_POST["pic_sesudah"],
                "batas_waktu_sesudah" => $_POST["batas_waktu_sesudah"],
                "status_sesudah" => $_POST["status_sesudah"],
                "keterangan_sesudah" => $_POST["keterangan_sesudah"],
                "step" => "1",
                "approval_status" => "Menunggu persetujuan MGR",
            );

            if ($this->model->edit($data, $_FILES)) {
                $response_data["success"] = true;
                $response_data["message"] = "Ubah data berhasil";
                header("refresh:2; url=" . base_url($this->controller_path . "/edit/" . $_POST['id']));
            } else {
                $response_data["success"] = false;
                $response_data["message"] = "Ubah data gagal";
            }
        } else if ($_POST["btn_submit"] == "setuju") {
            $data = array(
                "refid" => $_POST["id"],
                "type" => $this->step_type,
                "id_employee" => session()->get("id"),
                "komentar" => $_POST["komentar"]
            );

            $this->model->insertKomentar($data);

            if ($this->model->approve($_POST["id"])) {
                $response_data["success"] = true;
                $response_data["message"] = "Proses setuju berhasil";
                header("refresh:2; url=" . base_url($this->controller_path . "/list"));
            } else {
                $response_data["success"] = false;
                $response_data["message"] = "Proses setuju gagal";
                echo "<script>setTimeout(function() {history.back()}, 2000)</script>";
            }
        } else {
            $data = array(
                "refid" => $_POST["id"],
                "type" => $this->step_type,
                "id_employee" => session()->get("id"),
                "komentar" => $_POST["komentar"]
            );

            $this->model->insertKomentar($data);

            if ($this->model->cancel_approve($_POST["id"])) {
                $response_data["success"] = true;
                $response_data["message"] = "Proses kembalikan berhasil";
                header("refresh:2; url=" . base_url($this->controller_path . "/list"));
            } else {
                $response_data["success"] = false;
                $response_data["message"] = "Proses kembalikan gagal";
                echo "<script>setTimeout(function() {history.back()}, 2000)</script>";
            }
        }

        echo $response_data["message"];
    }

    public function process_delete($id)
    {

        if ($this->model->delete($id)) {
             header("refresh:2; url=". base_url() . "/" . $this->controller_path . "/" . "list");
        } else {
             header("refresh:2; url=". base_url() . "/" . $this->controller_path . "/" . "list");
        }

        echo $response_data["message"];
    }
}