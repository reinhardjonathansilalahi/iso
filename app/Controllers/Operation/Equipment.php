<?php namespace App\Controllers\Operation;

use App\Controllers\BaseTrackingController;
use App\FormHtmls\BasicFormHtml;
use App\FormObjects\operation\Equipment_form;
use App\Libraries\Tracking;
use App\Models\Operation\CalibrationLogModel;
use App\Models\Operation\EquipmentModel;
use App\Models\Operation\PreventiveMaintenanceModel;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class Equipment extends BaseTrackingController
{
    protected $step_type = 19;

    use Equipment_form;
    use BasicFormHtml;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        $this->tracking = new Tracking();
        $this->model = new EquipmentModel();

        $this->module_title = "Operation";
        $this->submodule_title = "Equipment";
        $this->controller_path = "operation/equipment";
        $this->controller_path_url = base_url($this->controller_path);
        $this->hasSupportingFiles = true;

        $this->view_data = array(
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path" => $this->controller_path,
            "controller_path_url" => $this->controller_path_url,
            "hasSupportingFiles" => $this->hasSupportingFiles,
        );
    }

    public function list()
    {
        $this->view_data["title"] = $this->submodule_title;
        $this->view_data["rows"] = $this->model->readAll(session()->get("id"));
        echo view("{$this->controller_path}_read", $this->view_data);
    }

    public function create()
    {
        $formArrayObject = $this->getFormObject();
        $formHtml = $this->getFormHTML($formArrayObject);

        $this->view_data["form"] = $formHtml;
        $this->view_data["formArrayObject"] = $formArrayObject;

        $this->view_data["title"] = "Create New {$this->submodule_title}";
        echo view("_base/tracking_create", $this->view_data);
    }

    public function edit($id)
    {
        $formArrayObject = $this->getFormObject();
        $formHtml = $this->getFormHTML($formArrayObject);

        $this->view_data["form"] = $formHtml;
        $this->view_data["formArrayObject"] = $formArrayObject;

        $this->view_data["title"] = "Edit {$this->submodule_title}";
        $this->view_data["row"] = $this->model->read($id);
        echo view("_base/tracking_edit", $this->view_data);
    }

    public function process_add()
    {
        $insertedIdEquipment = parent::process_add();
        $refresh_header = headers_list()[4];
        header_remove("Refresh");

        $data = array(
            "created_at" => "",
            "step" => "0",
            "approval_status" => "Menunggu persetujuan MGR",
        );

        foreach ($_POST as $key => $value) {
            $data[$key] = $value;
        }
        unset($data["btn_submit"]);

        $items = array();
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                if (array_depth($value) == 1) {
                    $array = $value;
                    $modified = implode("##", $array);
                    $data[$key] = $modified;
                } else if (array_depth($value) == 2) {
                    $items[$key] = $value;
                    unset($data[$key]);
                }
            }
        }

        $data["equipment"] = $insertedIdEquipment;

        // if checked
        if (!empty($data['calibration_check'])) {
            $this->addToCalibrationLog($data);
            $this->addToPreventiveMaintenance($data);
        } else {
            $this->addToPreventiveMaintenance($data);
        }

        header($refresh_header);
    }

    public function addToPreventiveMaintenance($data)
    {
        $step_type = 16;
        $model = new PreventiveMaintenanceModel();

        $isSuccess = $model->insert($data);
        $insertedId = $model->db->insertID();

        if (!empty($items)) {
            foreach ($items as $key => $value) {
                if (is_array($value)) {
                    $table_name = $key;
                    $array = $value;
                    foreach ($array as $dataObject) {
                        $insertedID = $isSuccess;
                        $dataObject["ref_id"] = $insertedID;
                        $model->insertItems($dataObject, $table_name);
                    }
                }
            }
        }

        $mgr = $this->tracking->mgr(session()->get("id"));
        $vp = $this->tracking->vp(session()->get("id"));
        $mr = $this->tracking->mr(session()->get("id"));

        // 16 -> step type
        $model->insertTracking(
            $this->tracking->insertTracking($insertedId, $step_type, 0, session()->get("id"))
        );
        $model->insertTracking(
            $this->tracking->insertTracking($insertedId, $step_type, 1, $mgr)
        );
        $model->insertTracking(
            $this->tracking->insertTracking($insertedId, $step_type, 2, $vp)
        );
        $model->insertTracking(
            $this->tracking->insertTracking($insertedId, $step_type, 3, $mr)
        );
    }

    public function addToCalibrationLog($data)
    {
        $step_type = 18;
        $model = new CalibrationLogModel();

        $isSuccess = $model->insert($data);
        $insertedId = $model->db->insertID();

        if (!empty($items)) {
            foreach ($items as $key => $value) {
                if (is_array($value)) {
                    $table_name = $key;
                    $array = $value;
                    foreach ($array as $dataObject) {
                        $insertedID = $isSuccess;
                        $dataObject["ref_id"] = $insertedID;
                        $model->insertItems($dataObject, $table_name);
                    }
                }
            }
        }

        $mgr = $this->tracking->mgr(session()->get("id"));
        $vp = $this->tracking->vp(session()->get("id"));
        $mr = $this->tracking->mr(session()->get("id"));

        // 16 -> step type
        $model->insertTracking(
            $this->tracking->insertTracking($insertedId, $step_type, 0, session()->get("id"))
        );
        $model->insertTracking(
            $this->tracking->insertTracking($insertedId, $step_type, 1, $mgr)
        );
        $model->insertTracking(
            $this->tracking->insertTracking($insertedId, $step_type, 2, $vp)
        );
        $model->insertTracking(
            $this->tracking->insertTracking($insertedId, $step_type, 3, $mr)
        );
    }

}
