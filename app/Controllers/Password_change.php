<?php namespace App\Controllers;

class Password_change extends BaseController {

    function __construct(){
        parent::__construct();
        $this->load->model('PasswordChangeModel');

        if (!user_access_check()){
			exit();
		}
    }

    function index(){
        if(session()->get('logged_in') == TRUE){

		  echo view('password_change_view');

        } else {
            // redirect(base_url());
        }
    }

    function process(){
        if(session()->get('logged_in') != TRUE){
            echo "Login session required";
            return;
        }

        $newPassword        = $this->input->post('newPassword', TRUE);
        $newPasswordRepeat  = $this->input->post('newPasswordRepeat', TRUE);

        if (empty($newPassword) || empty($newPasswordRepeat)){
            header("refresh:2; url=".base_url("password_change"));
            echo "Your new password cannot be empty";
            return;
        }

        $email              = session()->get('email');
        $oldPassword        = md5($this->input->post('oldPassword', TRUE));
        $newPassword        = md5($this->input->post('newPassword', TRUE));
        $newPasswordRepeat  = md5($this->input->post('newPasswordRepeat', TRUE));

        if ($newPassword != $newPasswordRepeat){
            header("refresh:2; url=".base_url("password_change"));
            echo "Your new password is not matching";
            return;
        }

        if($this->PasswordChangeModel->process($email, $oldPassword, $newPassword)){
            // redirect(base_url());
            header("refresh:2; url=".base_url());
            echo "Password is successfully changed";
        } else{
            // redirect('login');
            header("refresh:2; url=".base_url("password_change"));
            echo 'Old password is Wrong';
        }
    }

}
