<?php namespace App\Controllers\Sustainable_palmoil\Rspo;

use App\Controllers\BaseController;
use App\FormHtmls\BasicFormHtml;
use App\Models\SustainablePalmoil\RSPOModel;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class Indicator extends BaseController
{
    use BasicFormHtml;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        $this->model = new RSPOModel();

        $this->module_title = "Sustainable Palm Oil";
        $this->submodule_title = "";
        $this->controller_path = "sustainable_palmoil/rspo/indicator";
        $this->controller_path_url = base_url($this->controller_path);
        $this->hasSupportingFiles = true;

        $this->view_data = array(
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path" => $this->controller_path,
            "controller_path_url" => $this->controller_path_url,
            "hasSupportingFiles" => $this->hasSupportingFiles,
        );
    }

    public function list()
    {
        $main = $_GET['main'];
        $sub_main = $_GET['sub_main'];

        $this->view_data["title"] = $this->submodule_title;

        $this->view_data["rows_draft"] = $this->model->get_indicators(
            array("main" => $main, "sub_main" => $sub_main, "status" => "draft")
        );

        $this->view_data["rows_revision"] = $this->model->get_indicators(
            array("main" => $main, "sub_main" => $sub_main, "status" => "revision")
        );

        $this->view_data["rows_approved"] = $this->model->get_indicators(
            array("main" => $main, "sub_main" => $sub_main, "status" => "approved")
        );

        $this->view_data["totalCount"] =
            count($this->view_data["rows_draft"]) +
            count($this->view_data["rows_revision"]) +
            count($this->view_data["rows_approved"]);

        echo view("{$this->controller_path}_read", $this->view_data);
    }

}
