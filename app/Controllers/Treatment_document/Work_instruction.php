<?php namespace App\Controllers\Treatment_document;

use App\Controllers\BaseTrackingController;
use App\FormHtmls\BasicFormHtml;
use App\FormObjects\treatment_document\Work_instruction_form;
use App\Libraries\Tracking;
use App\Models\HR\DepartmentModel;
use App\Models\TreatmentDocument\WorkInstructionModel;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class Work_instruction extends BaseTrackingController
{
    protected $step_type = 1;

    use Work_instruction_form;
    use BasicFormHtml;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        $this->tracking = new Tracking();
        $this->model = new WorkInstructionModel();
        $this->DepartmentModel = new DepartmentModel();

        $this->module_title = "Treatment Document";
        $this->submodule_title = "Work Instruction";
        $this->controller_path = "treatment_document/work_instruction";
        $this->controller_path_url = base_url($this->controller_path);
        $this->hasSupportingFiles = true;

        $this->view_data = array(
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path" => $this->controller_path,
            "controller_path_url" => $this->controller_path_url,
            "hasSupportingFiles" => $this->hasSupportingFiles,
        );
    }

    public function list()
    {
        $this->view_data["title"] = $this->submodule_title;
        $this->view_data["rows"] = $this->model->readAll(session()->get("id"));
        echo view("{$this->controller_path}_read", $this->view_data);
    }

    public function create()
    {
        $formArrayObject = $this->getFormObject();
        $formHtml = $this->getFormHTML($formArrayObject);

        $this->view_data["form"] = $formHtml;
        $this->view_data["formArrayObject"] = $formArrayObject;

        $this->view_data["title"] = "Create New {$this->submodule_title}";
        echo view("_base/tracking_create", $this->view_data);
    }

    public function edit($id)
    {
        $formArrayObject = $this->getFormObject();
        $formHtml = $this->getFormHTML($formArrayObject);

        $this->view_data["form"] = $formHtml;
        $this->view_data["formArrayObject"] = $formArrayObject;

        $this->view_data["title"] = "Edit {$this->submodule_title}";
        $this->view_data["row"] = $this->model->read($id);
        echo view("_base/tracking_edit", $this->view_data);
    }


}


//public function pdf($id)
//{
//    $row = $this->model->readItem($id);
//
//    $data = array(
//        "job_no"        => $row->job_no,
//        "doc_name"        => $row->doc_name,
//        "project_number"=> $row->project_no,
//        "date_raised"   => $row->date_raised,
//        "work_description"=> $row->work_description,
//        "instruction"   => $row->instruction,
//        "responsibilities"=>$row->responsibilities,
//        "key_objectives"=> $row->key_objectives,
//        "procedures"    => $row->procedures,
//        "forms"         => $row->forms,
//        "standards"     => $row->standards,
//        "drawings"      => $row->drawings,
//        "tools"         => $row->tools,
//        "certification" => $row->certification,
//        "gauges"        => $row->gauges,
//        "prepared_by"   => get_ttd_val($row->prepared_by),
//        "position"   	=> get_approval_val($row->prepared_by),
//        "reviewed_by"   => get_ttd_val($row->reviewed_by),
//        "positions"   	=> get_approval_val($row->reviewed_by),
//    );
//
//    echo view("contoh", $data);
//}
//
//public function create()
//{
//    $data = array(
//        "title"			=> "Create New Treatment Document",
//        "module_title"  => $this->module_title,
//        "submodule_title"=> $this->submodule_title,
//        "controller_path_url" => $this->controller_path_url,
//    );
//    echo view("treatment_document/work_instruction_create", $data);
//}
//
//public function work_distribution($id)
//{
//    $data = array(
//        "title"			=> "Treatment Document",
//        "row" 			=> $this->model->dist($id)->row(),
//        "module_title"  => $this->module_title,
//        "submodule_title"=> $this->submodule_title,
//        "controller_path_url" => $this->controller_path_url,
//    );
//    echo view("treatment_document/work_distribusi_read", $data);
//}