<?php namespace App\FormEntities;

class FormRowType extends enum
{
    const TWO_SPLIT = 'two_split';
    const ONE_SOLO = 'one_solo';
    const ADDABLE = 'addable';
}