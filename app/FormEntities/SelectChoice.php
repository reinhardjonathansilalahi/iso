<?php namespace App\FormEntities;

class SelectChoice
{
    public $value;
    public $option;

    public $dependentToValueOf;
    public $dependantChoices;

    public function __construct($value = null, $option = null)
    {
        $this->value = $value;
        $this->option = $option;
    }

    public function setDependentToValueOf($formName){
        $this->dependentToValueOf = $formName;
    }
}