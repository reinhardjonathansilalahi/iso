<?php namespace App\FormObjects\context_organization;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Business_process_form
{
    private $deptLocationChoices;
    private $processTypeChoices;
    private $monitoringMethodologyChoices;
    private $inputCategoryChoices;
    private $outputCategoryChoices;
    private $riskImpactChoices;
    private $riskPossibilityChoices;
    private $riskCategoryChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            array(
                FormCol::createInstance(
                    FormColType::FORM_SELECT,
                    lang("context_organization.dept_location"),
                    "dept_location",
                    $this->deptLocationChoices),
                FormCol::createInstance(
                    FormColType::FORM_TEXTAREA,
                    lang("context_organization.process_name"),
                    "process_name"),
                FormCol::createInstance(
                    FormColType::FORM_SELECT,
                    lang("context_organization.process_type"),
                    "process_type",
                    $this->processTypeChoices),
                FormCol::createInstance(
                    FormColType::FORM_SELECT,
                    lang("context_organization.input_category"),
                    "input_category",
                    $this->inputCategoryChoices),
                FormCol::createInstance(
                    FormColType::FORM_TEXTAREA,
                    lang("context_organization.input"),
                    "input"),
                FormCol::createInstance(
                    FormColType::FORM_SELECT,
                    lang("context_organization.output_category"),
                    "output_category",
                    $this->outputCategoryChoices),
                FormCol::createInstance(
                    FormColType::FORM_TEXTAREA,
                    lang("context_organization.output"),
                    "output"),
            ),
            array(
                FormCol::createInstance(
                    FormColType::FORM_TEXTAREA,
                    lang("context_organization.performance_indicator"),
                    "performance_indicator"),
                FormCol::createInstance(
                    FormColType::FORM_SELECT,
                    lang("context_organization.monitoring_methodology"),
                    "monitoring_methodology",
                    $this->monitoringMethodologyChoices),
                FormCol::createInstance(
                    FormColType::FORM_TEXTAREA,
                    lang("context_organization.measurement"),
                    "measurement"),
                FormCol::createInstance(
                    FormColType::FORM_TEXTAREA,
                    lang("context_organization.resources"),
                    "resources"),
                FormCol::createInstance(
                    FormColType::FORM_TEXTAREA,
                    lang("context_organization.process_documented_information"),
                    "process_documented_information"),
                FormCol::createInstance(
                    FormColType::FORM_TEXTAREA,
                    lang("context_organization.person_in_charge"),
                    "person_in_charge"),
            )
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, "Create Risk",
            FormCol::createInstance(
                FormColType::FORM_TEXTAREA,
                lang("context_organization.risk_description"),
                "risk_description"),
            array(
                FormCol::createInstance(
                    FormColType::FORM_SELECT,
                    lang("context_organization.risk_possibility"),
                    "risk_possibility",
                    $this->riskPossibilityChoices),
                FormCol::createInstance(
                    FormColType::FORM_SELECT,
                    lang("context_organization.risk_impact"),
                    "risk_impact",
                    $this->riskImpactChoices),
                FormCol::createInstance(
                    FormColType::FORM_SELECT,
                    lang("context_organization.risk_category"),
                    "risk_category",
                    $this->riskCategoryChoices)
            )
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/monitoring_methodology_list");
        helper("select_option/dept_location_list");

        $this->deptLocationChoices = array();
        $choices = get_dept_location_list();
        foreach ($choices as $value => $option) {
            array_push($this->deptLocationChoices, new SelectChoice($value, $option));
        }

        $this->monitoringMethodologyChoices = array();
        $choices = get_monitoring_methodology_list();
        foreach ($choices as $value => $option) {
            array_push($this->monitoringMethodologyChoices, new SelectChoice($value, $option));
        }

        $this->processTypeChoices = array();
        $values = ["Main Process", "Sub Process"];
        foreach ($values as $item) {
            array_push($this->processTypeChoices, new SelectChoice($item, $item));
        }

        $this->inputCategoryChoices = array();
        $values = ["Internal", "External"];
        foreach ($values as $item) {
            array_push($this->inputCategoryChoices, new SelectChoice($item, $item));
        }
        $this->outputCategoryChoices = $this->inputCategoryChoices;


        //
        // ############### RISK #################
        //

        $this->riskImpactChoices = array();
        $values = ["Low", "Medium", "High"];
        foreach ($values as $item) {
            array_push($this->riskImpactChoices, new SelectChoice($item, $item));
        }

        $this->riskPossibilityChoices = $this->riskImpactChoices;

        $this->riskCategoryChoices = array();
        $riskCategories = $this->RiskOpportunityCategoryModel->findAll();
        foreach ($riskCategories as $item) {
            $value = $item->id;
            $option = $item->risk_category;
            array_push($this->riskCategoryChoices, new SelectChoice($value, $option));
        }
    }

}
