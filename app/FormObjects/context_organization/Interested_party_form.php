<?php namespace App\FormObjects\context_organization;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Interested_party_form
{
    private $interestedPartyChoices;
    private $managementSystemTypeChoices;
    private $approveAuthorChoices;
    private $riskImpactChoices;
    private $riskPossibilityChoices;
    private $riskCategoryChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

//        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
//            FormCol::createInstance(
//                FormColType::FORM_SELECT,
//                lang("context_organization.management_system_type"),
//                "management_system_type",
//                $this->managementSystemTypeChoices
//            )
//        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            array(
                FormCol::createInstance(
                    FormColType::FORM_SELECT,
                    lang("context_organization.stakeholder_interestedparty"),
                    "stakeholder_interestedparty",
                    $this->interestedPartyChoices
                ),
                FormCol::createInstance(
                    FormColType::FORM_TEXTAREA,
                    lang("context_organization.requirement_need_expectation"),
                    "requirement_need_expectation"
                ),
            ),
            array(
                FormCol::createInstance(
                    FormColType::FORM_TEXTAREA,
                    lang("context_organization.fulfillment"),
                    "fulfillment"
                ),
                FormCol::createInstance(
                    FormColType::FORM_SELECT,
                    lang("context_organization.approve_author"),
                    "approve_author",
                    $this->approveAuthorChoices
                )
            )
        ));

//        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, "Create Risk",
//            FormCol::createInstance(
//                FormColType::FORM_TEXTAREA,
//                lang("context_organization.risk_description"),
//                "risk_description"),
//            array(
//                FormCol::createInstance(
//                    FormColType::FORM_SELECT,
//                    lang("context_organization.risk_possibility"),
//                    "risk_possibility",
//                    $this->riskPossibilityChoices),
//                FormCol::createInstance(
//                    FormColType::FORM_SELECT,
//                    lang("context_organization.risk_impact"),
//                    "risk_impact",
//                    $this->riskImpactChoices),
//                FormCol::createInstance(
//                    FormColType::FORM_SELECT,
//                    lang("context_organization.risk_category"),
//                    "risk_category",
//                    $this->riskCategoryChoices)
//            )
//        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        $this->interestedPartyChoices = array();
        $values = [
            "Pelanggan ", "Karyawan", "Supplier",
            "Pemerintah", "Bankers", "Investor",
            "Local Community", "LSM", "Partner",
            "Owner (Shareholder)", "Asosiasi Industry", "Lembaga Sertifikasi",
            "Perusahaan yang lokasi berdekatan"];
        foreach ($values as $item) {
            array_push($this->interestedPartyChoices, new SelectChoice($item, $item));
        }

        $this->managementSystemTypeChoices = array();
        $managementSystemTypes = $this->ManagementSystemTypeModel->findAll();
        foreach ($managementSystemTypes as $item) {
            $value = $item->id;
            $option = $item->management_system_short_name;
            array_push($this->managementSystemTypeChoices, new SelectChoice($value, $option));
        }

        $this->approveAuthorChoices = array();
        $rows = $this->EmployeeModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->first_name . " " . $item->last_name;
            array_push($this->approveAuthorChoices, new SelectChoice($value, $option));
        }

        $this->riskImpactChoices = array();
        $values = ["Low", "Medium", "High"];
        foreach ($values as $item) {
            array_push($this->riskImpactChoices, new SelectChoice($item, $item));
        }

        $this->riskPossibilityChoices = $this->riskImpactChoices;

        $this->riskCategoryChoices = array();
        $riskCategories = $this->RiskOpportunityCategoryModel->findAll();
        foreach ($riskCategories as $item) {
            $value = $item->id;
            $option = $item->risk_category;
            array_push($this->riskCategoryChoices, new SelectChoice($value, $option));
        }
    }

}