<?php namespace App\FormObjects\context_organization;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Scope_form
{
    private $managementSystemTypeChoices;
    private $approveAuthorChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            array(
                FormCol::createInstance(
                    FormColType::FORM_SELECT,
                    lang("context_organization.management_system_type"),
                    "management_system_type",
                    $this->managementSystemTypeChoices
                ),
                FormCol::createInstance(
                    FormColType::FORM_TEXTAREA,
                    lang("context_organization.scope"),
                    "scope"
                ),
                FormCol::createInstance(
                    FormColType::FORM_TEXTAREA,
                    lang("context_organization.exclusion_justification"),
                    "exclusion_justification"
                ),
            ),
            array(
                FormCol::createInstance(
                    FormColType::FORM_TEXTAREA,
                    lang("context_organization.exclusion"),
                    "exclusion"
                ),
                FormCol::createInstance(
                    FormColType::FORM_SELECT,
                    lang("context_organization.approve_author"),
                    "approve_author",
                    $this->approveAuthorChoices
                )
            )
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        $this->managementSystemTypeChoices = array();
        $values = ["ISO 9001", "ISO 14001", "ISO 45001", "IMS", "RSPO", "ISPO", "Proper"];
        foreach ($values as $item) {
            array_push($this->managementSystemTypeChoices, new SelectChoice($item, $item));
        }

        $this->approveAuthorChoices = array();
        $rows = $this->EmployeeModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->first_name . " " . $item->last_name;
            array_push($this->approveAuthorChoices, new SelectChoice($value, $option));
        }
    }

}