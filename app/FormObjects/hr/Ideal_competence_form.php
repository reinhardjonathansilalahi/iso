<?php namespace App\FormObjects\hr;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormData;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Ideal_competence_form
{
    private $designationChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("hr.designation"),
                "designation",
                $this->designationChoices)
        ));

        $formRow = new FormRow(FormRowType::ADDABLE, lang("hr.ideal_competence"),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("hr.competence_type"),
                "competence_type"),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("hr.description"),
                "description"),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("hr.ideal_value"),
                "ideal_value")
        );
        $formRow->form_data = new FormData("hr_ideal_competence_items");
        array_push($formArrayObject, $formRow);

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        $this->approveAuthorChoices = array();
        $rows = $this->DesignationModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->designation_name;
            array_push($this->approveAuthorChoices, new SelectChoice($value, $option));
        }
    }

}
