<?php namespace App\FormObjects\internal_audit;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Audit_planning_form
{
    private $approveAuthorChoices;
    private $scheduleMonthChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, NULL,
            array(
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("internal_audit.dept_location"))
                    ->setFormName("dept_location"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_SELECT)
                    ->setFormLabel(lang("internal_audit.schedule_month"))
                    ->setFormName("schedule_month")
                    ->setFormData($this->scheduleMonthChoices)
            ),
            array(
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("internal_audit.auditor_name"))
                    ->setFormName("auditor_name"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("internal_audit.auditee_name"))
                    ->setFormName("auditee_name"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("internal_audit.location_audit"))
                    ->setFormName("location_audit"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_SELECT)
                    ->setFormLabel(lang("internal_audit.approve_author"))
                    ->setFormName("approve_author")
                    ->setFormData($this->approveAuthorChoices)
            )
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/schedule_month_list");

        $this->scheduleMonthChoices = array();
        $choices = get_schedule_month_list();
        foreach ($choices as $value => $option) {
            array_push($this->scheduleMonthChoices, new SelectChoice($value, $option));
        }

        $this->approveAuthorChoices = array();
        $rows = $this->EmployeeModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->first_name . " " . $item->last_name;
            array_push($this->approveAuthorChoices, new SelectChoice($value, $option));
        }
    }

}
