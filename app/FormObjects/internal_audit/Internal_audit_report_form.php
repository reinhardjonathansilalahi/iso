<?php namespace App\FormObjects\internal_audit;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Internal_audit_report_form
{
    private $approveAuthorChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, NULL,
            array(
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("internal_audit.dept_location"))
                    ->setFormName("dept_location"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT_DATE)
                    ->setFormLabel(lang("internal_audit.actual_date"))
                    ->setFormName("actual_date")
            ),
            array(
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("internal_audit.auditor"))
                    ->setFormName("auditor"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("internal_audit.auditee"))
                    ->setFormName("auditee"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("internal_audit.location"))
                    ->setFormName("location"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_SELECT)
                    ->setFormLabel(lang("internal_audit.approve_author"))
                    ->setFormName("approve_author")
                    ->setFormData($this->approveAuthorChoices)
            )
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, "Non Conformance",
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT_DATE)
                ->setFormLabel(lang("internal_audit.nc_date"))
                ->setFormName("nc_date"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("internal_audit.nc_status"))
                ->setFormName("nc_status")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, NULL,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("internal_audit.nc_detail"))
                ->setFormName("nc_detail")
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        $this->approveAuthorChoices = array();
        $rows = $this->EmployeeModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->first_name . " " . $item->last_name;
            array_push($this->approveAuthorChoices, new SelectChoice($value, $option));
        }
    }

}
