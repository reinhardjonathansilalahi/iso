<?php namespace App\FormObjects\operation;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait External_property_form
{
    private $approvalChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                array(
                    (new FormCol())
                        ->setFormColType(FormColType::FORM_INPUT_DATE)
                        ->setFormName("date")
                        ->setFormLabel(lang("operation.date")),
                    (new FormCol())
                        ->setFormColType(FormColType::FORM_INPUT)
                        ->setFormName("external_property_type")
                        ->setFormLabel(lang("operation.external_property_type")),
                    (new FormCol())
                        ->setFormColType(FormColType::FORM_INPUT)
                        ->setFormName("external_property_name")
                        ->setFormLabel(lang("operation.external_property_name")),
                    (new FormCol())
                        ->setFormColType(FormColType::FORM_INPUT)
                        ->setFormName("external_property_owner")
                        ->setFormLabel(lang("operation.external_property_owner")),
                ),
                array(
                    (new FormCol())
                        ->setFormColType(FormColType::FORM_INPUT)
                        ->setFormName("customer_name")
                        ->setFormLabel(lang("operation.customer_name")),
                    (new FormCol())
                        ->setFormColType(FormColType::FORM_INPUT)
                        ->setFormName("condition")
                        ->setFormLabel(lang("operation.condition")),
                    (new FormCol())
                        ->setFormColType(FormColType::FORM_TEXTAREA)
                        ->setFormName("remark")
                        ->setFormLabel(lang("operation.remark"))
                )
            )
        );

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/approval_list");

        $this->approvalChoices = array();
        $choices = get_approval_list();
        foreach ($choices as $value => $option) {
            array_push($this->approvalChoices, new SelectChoice($value, $option));
        }
    }

}
