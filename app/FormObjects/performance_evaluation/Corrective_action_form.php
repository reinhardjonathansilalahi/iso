<?php namespace App\FormObjects\performance_evaluation;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Corrective_action_form
{
    private $deptLocationChoices;
    private $internalExternalChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, NULL,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("performance_evaluation.dept_location"))
                ->setFormName("dept_location")
                ->setFormData($this->deptLocationChoices),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("performance_evaluation.team_leader"))
                ->setFormName("team_leader")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, NULL,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("performance_evaluation.description"))
                ->setFormName("description"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("performance_evaluation.team_member"))
                ->setFormName("team_member")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, NULL,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("performance_evaluation.internal_external_team"))
                ->setFormName("internal_external_team")
                ->setFormData($this->internalExternalChoices),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("performance_evaluation.approval"))
                ->setFormName("approval")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, " ",
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("performance_evaluation.describe_issue"))
                ->setFormName("describe_issue"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT_DATE)
                ->setFormLabel(lang("performance_evaluation.describe_issue_date"))
                ->setFormName("describe_issue_date")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, NULL,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("performance_evaluation.containment_plant"))
                ->setFormName("containment_plant"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT_DATE)
                ->setFormLabel(lang("performance_evaluation.containment_plant_date"))
                ->setFormName("containment_plant_date")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, NULL,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("performance_evaluation.root_causes"))
                ->setFormName("root_causes"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT_DATE)
                ->setFormLabel(lang("performance_evaluation.root_causes_date"))
                ->setFormName("root_causes_date")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, NULL,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("performance_evaluation.corrective_action"))
                ->setFormName("corrective_action"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT_DATE)
                ->setFormLabel(lang("performance_evaluation.corrective_action_date"))
                ->setFormName("corrective_action_date")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, NULL,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("performance_evaluation.preventive_action_plan"))
                ->setFormName("preventive_action_plan"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT_DATE)
                ->setFormLabel(lang("performance_evaluation.preventive_action_plan_date"))
                ->setFormName("preventive_action_plan_date")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, NULL,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("performance_evaluation.status"))
                ->setFormName("status"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT_DATE)
                ->setFormLabel(lang("performance_evaluation.status_date"))
                ->setFormName("status_date")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, " ",
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("performance_evaluation.note"))
                ->setFormName("note")
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/dept_location_list");

        $this->deptLocationChoices = array();
        $choices = get_dept_location_list();
        foreach ($choices as $value => $option) {
            array_push($this->deptLocationChoices, new SelectChoice($value, $option));
        }

        $this->internalExternalChoices = array();
        array_push($this->internalExternalChoices, new SelectChoice("Internal Team", "Internal Team"));
        array_push($this->internalExternalChoices, new SelectChoice("External Team", "External Team"));
    }

}
