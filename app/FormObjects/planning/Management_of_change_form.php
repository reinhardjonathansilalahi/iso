<?php namespace App\FormObjects\planning;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Management_of_change_form
{
    private $qhseRiskChoices;
    private $typeOfChangeChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("planning.proposed_changes"),
                "proposed_changes"),
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("planning.type_of_change"),
                "type_of_change",
                $this->typeOfChangeChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("planning.reason_purpose_of_change"),
                "reason_purpose_of_change"),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("planning.possible_impact_of_change"),
                "possible_impact_of_change")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("planning.resource_availability"),
                "resource_availability"),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("planning.approve_author"),
                "approve_author")
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/type_of_change_list");

        $this->typeOfChangeChoices = array();
        $choices = get_type_of_change_list();

        foreach ($choices as $value => $option) {
            array_push($this->typeOfChangeChoices, new SelectChoice($value, $option));
        }
    }

}
