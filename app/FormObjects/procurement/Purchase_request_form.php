<?php namespace App\FormObjects\procurement;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Purchase_request_form
{

    private $departmentChoices;
    private $documentTypeChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("procurement.departemen"))
                ->setFormName("departemen")
                ->setFormData($this->departmentChoices),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT_DATE)
                ->setFormLabel(lang("procurement.date"))
                ->setFormName("date")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.item_code"))
                ->setFormName("item_code")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.order_quantity"))
                ->setFormName("order_quantity")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.uom"))
                ->setFormName("uom")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("procurement.description"))
                ->setFormName("description")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.price_est"))
                ->setFormName("price_est")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT_DATE)
                ->setFormLabel(lang("procurement.required_date"))
                ->setFormName("required_date")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.monthly_usage"))
                ->setFormName("monthly_usage")
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/document_type_list");

        $this->documentTypeChoices = array();
        $choices = get_document_type_list();
        foreach ($choices as $value => $option) {
            array_push($this->documentTypeChoices, new SelectChoice($value, $option));
        }

        // Department Choice
        $this->departmentChoices = array();
        $rows = $this->DepartmentModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->department_name;
            array_push($this->departmentChoices, new SelectChoice($value, $option));
        }
    }

}
