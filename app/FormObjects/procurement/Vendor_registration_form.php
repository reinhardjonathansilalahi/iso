<?php namespace App\FormObjects\procurement;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Vendor_registration_form
{

    private $companyTypeChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            array(
                (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("procurement.company_type"))
                ->setFormName("company_type")
                ->setFormData($this->companyTypeChoices),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("procurement.vendor_name"))
                    ->setFormName("vendor_name"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("procurement.directore_name"))
                    ->setFormName("directore_name"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_TEXTAREA)
                    ->setFormLabel(lang("procurement.address"))
                    ->setFormName("address")
            ),
            array(
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("procurement.phone_number"))
                    ->setFormName("phone_number"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("procurement.email"))
                    ->setFormName("email"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("procurement.npwp_number"))
                    ->setFormName("npwp_number"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_FILE)
                    ->setFormName("npwp_file"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("procurement.nib_number"))
                    ->setFormName("nib_number"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_FILE)
                    ->setFormName("nib_file")
            )
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/document_type_list");

        $this->companyTypeChoices = array();
        $choices = array(
            "CV", "PT", "Koperasi", "Yayasan", "Perseorangan"
        );

        foreach ($choices as $item) {
            array_push($this->companyTypeChoices, new SelectChoice($item, $item));
        }

    }

}
