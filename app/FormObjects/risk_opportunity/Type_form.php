<?php namespace App\FormObjects\risk_opportunity;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;

trait Type_form
{

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("risk_opportunity.risk_type"))
                ->setFormName("risk_type")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("risk_opportunity.remarks"))
                ->setFormName("remarks")
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {

    }

}
