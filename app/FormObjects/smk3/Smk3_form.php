<?php namespace App\FormObjects\smk3;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;

trait Smk3_form
{

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                "Deskripsi",
                "description")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            FormCol::createInstance(
                FormColType::FORM_TEXTAREA,
                "Panduan (Toggle)",
                "description_expand")
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {

    }

}
