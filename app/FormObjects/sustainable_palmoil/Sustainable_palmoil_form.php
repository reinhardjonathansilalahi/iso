<?php namespace App\FormObjects\sustainable_palmoil;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Sustainable_palmoil_form
{

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        $ratings = array();
        $values = ["0", "1", "2"];
        foreach ($values as $item) {
            array_push($ratings, new SelectChoice($item, $item));
        }

        $rightSide =  FormCol::createInstance();
        if (session()->get('id') == 3) { // mgr
            $rightSide = FormCol::createInstance(
                FormColType::FORM_SELECT,
                "Rating",
                "rating",
                $ratings);
        }

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                "Document Title",
                "doc_title"),
            $rightSide
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {

    }

}
