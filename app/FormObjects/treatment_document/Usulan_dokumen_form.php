<?php namespace App\FormObjects\treatment_document;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormData;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;

trait Usulan_dokumen_form
{
    private $departmentChoices;
    private $approveAuthorChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("treatment_document.header_doc_no"))
                ->setFormName("header_doc_no"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("treatment_document.header_rev_no"))
                ->setFormName("header_rev_no")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("treatment_document.nama_pengusul"))
                ->setFormName("nama_pengusul"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("treatment_document.nama_bagian"))
                ->setFormName("nama_bagian")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("treatment_document.judul_dokumen"))
                ->setFormName("judul_dokumen"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("treatment_document.doc_no"))
                ->setFormName("doc_no")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("treatment_document.versi"))
                ->setFormName("versi")
        ));

        $formRow = new FormRow(FormRowType::ADDABLE, NULL,
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("treatment_document.no_halaman"),
                "no_halaman"),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("treatment_document.no_bagian"),
                "no_bagian"),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("treatment_document.klausul_awal_1"),
                "klausul_awal_1"),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("treatment_document.klausul_awal_2"),
                "klausul_awal_2"),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("treatment_document.klausul_perubahan_1"),
                "klausul_perubahan_1"),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("treatment_document.klausul_perubahan_2"),
                "klausul_perubahan_2")
        );
        $formRow->form_data = new FormData("treatment_usulan_dokumen_items");
        array_push($formArrayObject, $formRow);

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
//        helper("select_option/document_type_list");
//
//        $this->documentTypeChoices = array();
//        $choices = get_document_type_list();
//        foreach ($choices as $value => $option) {
//            array_push($this->documentTypeChoices, new SelectChoice($value, $option));
//        }

//        // Department Choice
//        $this->departmentChoices = array();
//        $rows = $this->DepartmentModel->findAll();
//        foreach ($rows as $item) {
//            $value = $item->id;
//            $option = $item->department_name;
//            array_push($this->departmentChoices, new SelectChoice($value, $option));
//        }
//
//        // Approve Author Choice
//        $this->approveAuthorChoices = array();
//        $rows = $this->EmployeeModel->findAll();
//        foreach ($rows as $item) {
//            $value = $item->id;
//            $option = $item->first_name . " " . $item->last_name;
//            array_push($this->approveAuthorChoices, new SelectChoice($value, $option));
//        }
    }

}
