<?php

if ( ! function_exists('filter_output'))
{
    function filter_output($argVal = '')
    {

        // Filtering new line, untuk val yang ada enternya, itu mengandung \r\n
        // NOTE: \r itu artinya return dan \n artinya newline
        $filter = str_replace(array("\r\n"), '\n', $argVal);

        return $filter;
    }
}
