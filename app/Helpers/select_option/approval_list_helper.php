<?php

if ( ! function_exists('get_approval_list'))
{
    function get_approval_list()
    {
        // "value" => "option"
        return array(
            "Director" => "Director",
            "GM" => "GM",
            "Manager/Head" => "Manager/Head",
            "Supervisor" => "Supervisor",
            "Other" => "Other"
        );
    }
}