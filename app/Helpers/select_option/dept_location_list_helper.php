<?php

if ( ! function_exists('get_dept_location_list'))
{
    function get_dept_location_list()
    {
        // "value" => "option"
        return array(
            "HRD" => "HRD",
            "Operation" => "Operation",
            "Finance" => "Finance",
            "Workshop" => "Workshop",
            "Infrastructure" => "Infrastructure",
            "Marketing" => "Marketing",
            "HSE" => "HSE"
        );
    }
}