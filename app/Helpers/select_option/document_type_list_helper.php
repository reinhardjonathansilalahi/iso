<?php

if ( ! function_exists('get_document_type_list'))
{
    function get_document_type_list()
    {
        // "value" => "option"
        return array(
            "Checklist" => "Checklist",
            "Flowchart" => "Flowchart",
            "Form" => "Form",
            "Manual" => "Manual",
            "Policy" => "Policy",
            "Procedure" => "Procedure",
            "Work Instruction" => "Work Instruction",
        );
    }
}