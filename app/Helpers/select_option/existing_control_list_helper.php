<?php

if ( ! function_exists('get_existing_control_list'))
{
    function get_existing_control_list()
    {
        // "value" => "option"
        return array(
            "Elimination" => "Elimination",
            "Subtitution" => "Subtitution",
            "Engineering" => "Engineering",
            "Administration" => "Administration",
            "PPE / Tools" => "PPE / Tools",
        );
    }
}