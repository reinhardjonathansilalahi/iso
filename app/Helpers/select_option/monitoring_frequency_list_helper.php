<?php

if ( ! function_exists('get_monitoring_frequency_list'))
{
    function get_monitoring_frequency_list()
    {
        // "value" => "option"
        return array(
            "HY" => "Half Yearly",
            "M" => "Monthly",
            "Q" => "Quarterly",
            "W" => "Weekly",
            "Y" => "Yearly"
        );
    }
}