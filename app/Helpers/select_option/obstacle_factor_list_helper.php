<?php

if (!function_exists('get_obstacle_factor_list')) {
    function get_obstacle_factor_list()
    {
        // "value" => "option"
        return array(
            "Men, not competent" => "Men, not competent",
            "Men, human error" => "Men, human error",
            "Men, improper motivation" => "Men, improper motivation",
            "Men, lack of experience" => "Men, lack of experience",
            "Methode, no work guidance" => "Methode, no work guidance",
            "Methode, not applicable work guidance" => "Methode, not applicable work guidance",
            "Mehtode, no socialized" => "Mehtode, no socialized",
            "Material, incorrect material" => "Material, incorrect material",
            "Material, purchase error" => "Material, purchase error",
            "Material, didn’t meet the requirement standard" => "Material, didn’t meet the requirement standard",
            "Tools, damage" => "Tools, damage",
            "Tools, lack of amount" => "Tools, lack of amount",
            "Tools, not standard" => "Tools, not standard",
        );
    }
}