<?php

if ( ! function_exists('get_status_list'))
{
    function get_status_list()
    {
        // "value" => "option"
        return array(
            "Good" => "Good",
            "Service" => "Service",
            "Replacement" => "Replacement"
        );
    }
}