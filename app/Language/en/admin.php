<?php
return [
    "action" => "Action",
    "data_table" => "Data Table",
    "nav_dashboard" => "Dashboard",
    "nav_business_process" => "Business Proces",
    "reference" => "Reference",

    "procedure" => "Procedure",
    "document_no" => "Document No",
    "rev_no" => "Rev No.",
    "header_title" => "Header Title",
    "distribusi" => "Distribution",

    # Admin - Site
    "management_system_type" => "Management System Type",
    "site" => "Site",
    "site_name" => "Site Name",
    "address" => "Address",
    "facilities" => "Facilities",
    "operations" => "Operations",
    "remarks" => "Remarks",
    "management_system_short" => "Management System Short Name",
    "management_system_long" => "Management System Long Name",
    "requirements_needs_expectations" => "Requirements, Needs and Expectations",
    "external_issue" => "External Issues",
    "internal_issue" => "Internal Issues",
    "interested_party" => "Interested Party",
    "stakeholder" => "Stakeholder",
    "impact_on_organization" => "Impact on Organization",
    "department" => "Department",

    # Admin Management System Type
    "management_system_short_name" => "Management System Short Name",
    "management_system_full_name" => "Management System Full Name",
];