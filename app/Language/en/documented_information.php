<?php
return [
    "action" => "Action",
    "data_table" => "Data Table",
    "nav_dashboard" => "Dashboard",
    "nav_business_process" => "Business Proces",
    "reference" => "Reference",
    "procedure" => "Procedure",
    "document_no" => "Document No",
    "rev_no" => "Rev No.",
    "header_title" => "Header Title",
    "distribusi" => "Distribution",
    "approve_author" => "Approving Authority",
    "approval_status" => "Approval Status",
    "supporting_files" => "Supporting Files",

    # Documents
    "management_system_type" => "Management System Type",
    "department" => "Department",
    "document_name" => "Document Name",
    "revision_number" => "Revision Number",
    "document_category" => "Document Category",
    "document_type" => "Document Type",
    "next_revision_date" => "Next Revision Date",
    "document" => "Document",
    "revision_date" => "Revision Date",

    # External Document
    "reference_number" => "Reference Number",
    "origin" => "Origin",
    "receipt_date" => "Receipt Date",
    "document_link" => "Document Link",
    "remarks" => "Remarks",
];