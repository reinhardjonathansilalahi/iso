<?php
return [
    "action" => "Action",
    "data_table" => "Data Table",
    "nav_dashboard" => "Dashboard",
    "nav_business_process" => "Business Proces",
    "reference" => "Reference",
    "procedure" => "Procedure",
    "document_no" => "Document No",
    "rev_no" => "Rev No.",
    "header_title" => "Header Title",
    "distribusi" => "Distribution",
    "approve_author" => "Approving Authority",
    "approval_status" => "Approval Status",
    "supporting_files" => "Supporting Files",

    # Non conformance
    "no" => "Number",
    "departemen" => "Department",
    "lokasi" => "Location",
    "audit" => "Audit",
    "tanggal" => "Date",
    "penjelasan_ketidaksesuaian" => "Penjelasan Tentang Ketidaksesuaian",
    "tindakan_perbaikan" => "Tindakan Perbaikan",
    "tanggal_penyelesaian" => "Tanggal Penyelesaian",
    "penjelasan_perbaikan" => "Penjelasan Perbaikan",
    "tindakan_perbaikan_selesai" => "Tindakan Perbaikan Selesai",

    # Audit Planning
    "dept_location" => "Dept / Location",
    "schedule_month" => "Schedule Month",
    "auditor_name" => "Auditor Name",
    "auditee_name" => "Auditee Name",
    "location_audit" => "Location Audit",

    # Internal Audit Report
    "actual_date" => "Actual Date",
    "auditor" => "Auditor",
    "auditee" => "Auditee",
    "location" => "Location",
    "nc_date" => "NC Date",
    "nc_status" => "NC Status",
    "nc_detail" => "NC Detail",
];