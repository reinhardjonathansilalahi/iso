<?php
return [
    # Calibration Log
    "equipment_name" => "Equipment Name",
    "equipment_owner" => "Equipment Owner",
    "equipment_location" => "Equipment Location",
    "model_name" => "Model Name",
    "identification_number" => "Identification Number",
    "asset_number" => "Asset Number",
    "date_of_last_calibration" => "Date of Last Calibration",
    "next_calibration_date" => "Next Calibration Date",

    # Equipment
    "equipment_name" => "Equipment Name",
    "equipment_owner" => "Equipment Owner",
    "equipment_location" => "Equipment Location",
    "model_name" => "Model Name",
    "identification_number" => "Identification Number",
    "asset_number" => "Asset number",
    "date_of_purchase" => "Date of Purchase",
    "status" => "Status",

    # Management of Change
    "propose_change" => "Propose Change",
    "reason_for_change" => "Reson for Change",
    "type_of_proposed_change" => "Type of Proposed Change",
    "possible_impact_of_change" => "Possible Impact of Change",
    "remark" => "Remark",

    # External Property
    "date" => "Date",
    "customer_name" => "Customer / Supplier Name",
    "external_property_name" => "External Property Name",
    "external_property_type" => "External Property Type",
    "external_property_owner" => "External Property Owner",
    "condition" => "Condition",

    # Preventive Maintenance
    "equipment" => "Equipment",
    "frequency" => "Frequency",
    "item" => "Item",
    "activity" => "Activity",
    "responsibility" => "Responsibility",
    "remark" => "Remark",

    # Enquiry
    "enquiry_date" => "Enquiry Date",
    "phone" => "Phone",
    "prospect_company" => "Prospect Company",
    "website" => "Website",
    "address" => "Address",
    "prospect_name" => "Prospect Name",
    "city" => "City",
    "email" => "Email",
    "remark" => "Remark"
];