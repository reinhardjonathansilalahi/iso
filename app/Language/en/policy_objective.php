<?php
return [
    "action" => "Action",
    "data_table" => "Data Table",
    "nav_dashboard" => "Dashboard",
    "nav_business_process" => "Business Proces",
    "reference" => "Reference",
    "procedure" => "Procedure",
    "document_no" => "Document No",
    "rev_no" => "Rev No.",
    "header_title" => "Header Title",
    "distribusi" => "Distribution",
    "approve_author" => "Approving Authority",
    "approval_status" => "Approval Status",
    "supporting_files" => "Supporting Files",

    # Policy
    "management_system_type" => "Management System Type",
    "department" => "Department",
    "policy_statement" => "Policy Statement",
    "remarks" => "Remarks",
    "created_at" => "Created at",

    # Objective
    "objective_start_date" => "Objective Start Date",
    "objective_end_date" => "Objective End Date",
    "monitoring_frequency" => "Monitoring Frequency",
    "monitoring_methodology" => "Monitoring Methodology",
    "objective" => "Objective",
    "access_control" => "Access Control",
];