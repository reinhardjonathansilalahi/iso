<?php namespace App\Models\Admin;

use CodeIgniter\Model;

class LogoModel extends Model {

    protected $table = "logo";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["url", "active"];

//    function read(){
//        $select = $this->db->query("SELECT * FROM logo");
//        return $select;
//    }
//
//    function insert($data, $file){
//        if (!empty($file["file_upload"]['name'])){
//            $file_upload = upload_file($file);
//
//            if ($file_upload['status'] == "success"){
//                $file_url = base_url("assets/uploaded_files/") . $file_upload['filename'];
//            }
//            else if ($file_upload['status'] == "failed") {
//                return false;
//            }
//        }
//
//        $data = array(
//            "active"         	        => $data['active'],
//            "url"                       => empty($file_url) ? "" : $file_url
//        );
//
//        $this->db->insert("logo", $data);
//        return true;
//    }
//
//    function readItem($id){
//        $query = $this->db->query("SELECT * FROM logo where id='$id'");
//        $row = $query->row();
//        return $row;
//    }
//
//    function logoActive(){
//        $query = $this->db->query("SELECT * FROM logo where active = 1 limit 1");
//        $row = $query->row();
//        return $row;
//    }
//
//    function delete($id){
//        $row = $this->readItem($id);
//
//        if (!empty($row->file_url)){
//            delete_file_url($row->file_url);
//        }
//
//        $this->db->where('id', $id);
//        $result = $this->db->delete('logo');
//        return $result;
//    }
}