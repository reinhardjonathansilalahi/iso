<?php namespace App\Models\Admin;

use App\Models\_base\BaseModel;

class SiteModel extends BaseModel
{

    protected $table = "admin_site";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["site_name", "facilities", "address",
        "operations", "remarks"];

}

?>
