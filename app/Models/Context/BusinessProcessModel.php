<?php namespace App\Models\Context;

use App\Models\_base\BaseModel;

class BusinessProcessModel extends BaseModel
{
    protected $table = "context_business_process";
    protected $primaryKey = "id";
    protected $returnType = 'object';
    protected $allowedFields = ["dept_location", "process_name", "process_type", "input_category", "input",
        "output_category", "output", "performance_indicator", "monitoring_methodology", "measurement",
        "resources", "process_documented_information", "person_in_charge", "risk_description",
        "risk_category", "risk_impact", "risk_possibility", "file_url", "approval_status"];

//
//    public function read($id)
//    {
//        $select = $this->db->query("SELECT * FROM {$this->table_name}");
//        return $select;
//    }
//
//    public function readItem($id)
//    {
//        $query = $this->db->query("SELECT * FROM {$this->table_name} WHERE id=$id");
//        $row = $query->row();
//        return $row;
//    }
//
//    function edit($input, $file)
//    {
//        $row = $this->readItem($input["id"]);
//        if (!empty($file["file_upload"]["name"])) {
//            $file_upload = upload_file($file);
//
//            if ($file_upload["status"] == "success") {
//                delete_file_url($row->file_url);
//                $file_url = base_url("assets/uploaded_files/") . $file_upload["filename"];
//            } else if ($file_upload["status"] == "failed") {
//                return false;
//            }
//        }
//
//        $data = array(
//            "file_url" => empty($file_url) ? $row->file_url : $file_url,
//        );
//
//        foreach ($input as $key => $value) {
//            $data[$key] = $value;
//        }
//
//        $this->db->where("id", $input["id"]);
//        return $this->db->update("{$this->table_name}", $data);
//    }
//
//    function delete($id)
//    {
//
//        $row = $this->readItem($id);
//        if (!empty($row->file_url)) {
//            delete_file_url($row->file_url);
//        }
//
//        $this->db->where("id", $id);
//        $result = $this->db->delete("{$this->table_name}");
//
//        return $result;
//    }

}

?>
