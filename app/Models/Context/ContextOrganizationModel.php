<?php namespace App\Models\Context;

use CodeIgniter\Model;

class ContextOrganizationModel extends Model
{

    protected $table = "context_organization";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["where_aim", "what_do", "why_good",
        "department", "site", "approve_author", "swot_strength",
        "swot_weakness", "swot_opportunity", "swot_threat",
        "pestel_political", "pestel_economic", "pestel_social",
        "pestel_technological", "pestel_legal", "pestel_environmental",
        "file_url"];

}

?>
