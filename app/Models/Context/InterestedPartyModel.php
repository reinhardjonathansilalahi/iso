<?php namespace App\Models\Context;

use App\Models\_base\BaseTrackingModel;

class InterestedPartyModel extends BaseTrackingModel
{
    protected $table = "context_interested_party";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["management_system_type", "stakeholder_interestedparty", "fulfillment",
        "requirement_need_expectation", "impact", "risk_description", "risk_category",
        "risk_impact", "risk_possibility", "approve_author", "created_at", "file_url",
        "step", "approval_status"];

    protected $step_type = 11;

}

?>
