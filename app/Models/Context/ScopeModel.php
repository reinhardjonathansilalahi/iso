<?php namespace App\Models\Context;

use App\Models\_base\BaseTrackingModel;

class ScopeModel extends BaseTrackingModel
{

    protected $table = "context_scope";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["management_system_type", "scope", "exclusion",
        "exclusion_justification", "approve_author", "created_at", "file_url",
        "step", "approval_status"];

    protected $step_type = 12;

}

?>
