<?php namespace App\Models\DocumentedInformation;

use App\Models\_base\BaseModel;

class ExternalDocumentModel extends BaseModel
{

    protected $table = "documented_external";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["department", "document_name", "revision_number",
        "reference_number", "origin", "receipt_date", "document_link",
        "remarks", "file_url"];

}

?>
