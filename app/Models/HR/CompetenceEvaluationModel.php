<?php namespace App\Models\HR;

use CodeIgniter\Model;

class CompetenceEvaluationModel extends Model {



	function insert($data_pass){
        $id = $data_pass['id'];
        $datas = $data_pass['datas'];

        foreach ($datas as $data) {
            $array = array(
                    "id"                        => NULL,
                    "items_id"                  => $id,
                    "no"                        => $data['no'],
                    "remarks"                   => $data['remarks'],
                    "current_description"       => $data['current_description'],
                    "current_value"             => $data['current_value']
            );

            $this->db->insert('hr_competence_evaluation_items', $array);
        }

        return true;
    }

    function read(){
        $select = $this->db->query("SELECT DISTINCT id, designation_id FROM hr_ideal_competence");
        return $select;
    }

    function readItems($id){
        // $query = $this->db->query("SELECT * FROM hr_ideal_competence_items where items_id='$id'");
        $query = $this->db->query("SELECT b.designation_id, a.items_id, a.no, a.competence_type, a.description, a.ideal_value
            FROM hr_ideal_competence_items AS a
            JOIN hr_ideal_competence AS b
            ON a.items_id = b.id
            WHERE b.id = $id");
        // $row = $query->row();
        return $query;
    }

    function readEvaluationItems($id){
        // $query = $this->db->query("SELECT * FROM hr_ideal_competence_items where items_id='$id'");
        $query = $this->db->query("SELECT b.designation_id, a.items_id, a.no, a.remarks, a.current_description, a.current_value
            FROM hr_competence_evaluation_items AS a
            JOIN hr_ideal_competence AS b
            ON a.items_id = b.id
            WHERE b.id = $id");
        // $row = $query->row();
        return $query;
    }

    function edit($data_pass){
        $id = $data_pass['id'];
        $datas = $data_pass['datas'];

        $select = $this->db->query("SELECT * FROM hr_competence_evaluation_items where items_id='$id'");

        if ($select->num_rows() == 0){
            $this->insert($data_pass);
            return true;
        } else {

            // $designation_id = $datas[0]['designation_id'];

            foreach ($datas as $data) {
                $array = array(
                        "remarks"                   => $data['remarks'],
                        "current_description"       => $data['current_description'],
                        "current_value"             => $data['current_value']
                );

                $this->db->where(array(
                    'items_id'          => $id,
                    'no'                => $data['no'],
                ));

                $this->db->update('hr_competence_evaluation_items', $array);
            }

            return true;
        }
    }

    function delete($id){
        $this->db->where('id', $id);
        $this->db->delete('hr_ideal_competence');

        $this->db->where('items_id', $id);
        $result = $this->db->delete('hr_ideal_competence_items');
        return $result;
    }

}
?>
