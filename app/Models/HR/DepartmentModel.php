<?php namespace App\Models\HR;

use App\Models\_base\BaseModel;

class DepartmentModel extends BaseModel
{

    protected $table = "hr_department";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["department_name", "profile",
        "department_hod"];

}

?>
