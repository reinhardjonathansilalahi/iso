<?php namespace App\Models\HR;

use App\Models\_base\BaseModel;

class DesignationModel extends BaseModel
{

    protected $table = "hr_designation";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["designation_name", "approve_author", "created_at"];

}

?>
