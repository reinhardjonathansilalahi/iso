<?php namespace App\Models\HR;

use App\Models\_base\BaseModel;

class EmployeeModel extends BaseModel
{

    protected $table = "hr_employee";
    protected $primaryKey = "id";
    protected $returnType = 'object';
    protected $allowedFields = ["user_title", "first_name", "last_name", "date_of_birth",
        "joining_date", "designation", "department", "jabatan", "role", "area", "employee_status",
        "email", "phone_number", "work_location", "remarks", "activate_login", "url"];

    function getDepartment($id)
    {
        $query = $this->db->query("SELECT department FROM hr_employee where id='$id'");
        $row = $query->getRow();
        return $row->department;
    }

}

?>
