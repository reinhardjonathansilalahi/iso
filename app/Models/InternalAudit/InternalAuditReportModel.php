<?php namespace App\Models\InternalAudit;

use App\Models\_base\BaseTrackingModel;

class InternalAuditReportModel extends BaseTrackingModel
{
    protected $table = "internal_audit_internal_audit_report";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["dept_location", "actual_date", "auditor", "auditee",
        "location", "nc_date", "nc_detail", "nc_status",
        "approve_author", "file_url", "step", "approval_status"];

    protected $step_type = 25;

}