<?php namespace App\Models\ManagementReview;

use App\Models\_base\BaseTrackingModel;

class ManagementReviewResultModel extends BaseTrackingModel
{
    protected $table = "internal_audit_management_review_result";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["actual_management_review", "approve_author", "attendee",
        "file_url", "step", "approval_status"];

    protected $step_type = 23;

    public function getItems($table_name, $ref_id)
    {
        return $this->db->query("SELECT action_required, responsibility, target_date, actual_date FROM $table_name WHERE ref_id=$ref_id")->getResultArray();
    }

    public function insertItems($data, $table_name)
    {
        $this->db->table($table_name)->insert($data);
    }

    public function deleteItems($ref_id, $table_name)
    {
        return $this->db->table($table_name)->delete(array("ref_id" => $ref_id));
    }

}