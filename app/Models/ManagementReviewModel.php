<?php namespace App\Models;

use CodeIgniter\Model;

class ManagementReviewModel extends Model
{

    private $step_type = 7;
    private $table_name = "management_review";
    
    #  CRUD
    # --------------------------------------------------------------------------------

    public function insert($data)
    {
        $data = array(
                "id" => null,
                "hari_tanggal" => $data['hari_tanggal'],
                "waktu" => $data['waktu'],
                "tempat" => $data['tempat'],
                "topik_sebelum" => $data['topik_sebelum'],
                "uraian_sebelum" => $data['uraian_sebelum'],
                'pic_sebelum' => $data['pic_sebelum'],
                "batas_waktu_sebelum" => $data['batas_waktu_sebelum'],
                "status_sebelum" => $data['status_sebelum'],
                'keterangan_sebelum' => $data['keterangan_sebelum'],
                "topik_sesudah" => $data['topik_sesudah'],
                "uraian_sesudah" => $data['uraian_sesudah'],
                'pic_sesudah' => $data['pic_sesudah'],
                "batas_waktu_sesudah" => $data['batas_waktu_sesudah'],
                "status_sesudah" => $data['status_sesudah'],
                'keterangan_sesudah' => $data['keterangan_sesudah'],
                "step" => $data["step"],
                "approval_status" => $data["approval_status"],
            );

        $this->db->insert($this->table_name, $data);
        $last = $this->db->insert_id();
        return $last;
    }

    public function insertPenerima($refid, $type, $dep)
    {
        $data = array(
                "refid"         => $refid,
                "type"          => $type,
                "id_department" => $dep
           );

        return $this->db->insert("distribusi", $data);
    }

    public function insertTracking($data)
    {
        $data = array(
                "refid"         => $data['refid'],
                "type"          => $data['type'],
                "step"    		  => $data['step'],
                "id_employee"   => $data['id_employee'],
           );

        return $this->db->insert("tracking", $data);
    }

    public function read($id)
    {
        $select = $this->db->query("SELECT tp.* FROM {$this->table_name} tp
        							JOIN tracking t ON tp.step = t.step AND tp.id = t.refid
    									WHERE t.type = $this->step_type AND t.id_employee = $id");
        return $select;
    }

    public function readItem($id)
    {
        $query = $this->db->query("SELECT tp.*, t.id_employee FROM {$this->table_name} tp
        							JOIN tracking t ON tp.step = t.step AND tp.id = t.refid
									WHERE t.type = $this->step_type  AND tp.id = ".$id);
        $row = $query->row();
        return $row;
    }

    public function edit($data_pass)
    {
        $data = array(
            "hari_tanggal" => $data_pass['hari_tanggal'],
            "waktu" => $data_pass['waktu'],
            "tempat" => $data_pass['tempat'],
            "topik_sebelum" => $data_pass['topik_sebelum'],
            "uraian_sebelum" => $data_pass['uraian_sebelum'],
            'pic_sebelum' => $data_pass['pic_sebelum'],
            "batas_waktu_sebelum" => $data_pass['batas_waktu_sebelum'],
            "status_sebelum" => $data_pass['status_sebelum'],
            'keterangan_sebelum' => $data_pass['keterangan_sebelum'],
            "topik_sesudah" => $data_pass['topik_sesudah'],
            "uraian_sesudah" => $data_pass['uraian_sesudah'],
            'pic_sesudah' => $data_pass['pic_sesudah'],
            "batas_waktu_sesudah" => $data_pass['batas_waktu_sesudah'],
            "status_sesudah" => $data_pass['status_sesudah'],
            'keterangan_sesudah' => $data_pass['keterangan_sesudah'],
            "step" => $data_pass["step"],
            "approval_status" => $data_pass["approval_status"],
        );

        $this->db->where('id', $data_pass['id']);
        return $this->db->update($this->table_name, $data);
    }

    public function approve($id)
    {
        $query = $this->db->query("SELECT t.step
					FROM {$this->table_name} twi
					JOIN tracking t ON twi.id = t.refid AND twi.step = t.step
					WHERE t.type = $this->step_type AND twi.id =  ".$id)->row();

        $data = array(
            "step" => $query->step + 1,
        );

        if ($data["step"] == 2){
            $data["approval_status"] = "Menunggu persetujuan VP";
        } else if ($data["step"] == 3){
            $data["approval_status"] = "Disetujui";
        } else {
            $data["approval_status"] = "Menunggu persetujuan";
        }

        $this->db->where('id', $id);
        return $this->db->update($this->table_name, $data);
    }

    public function cancel_approve($id)
    {
        $query = $this->db->query("SELECT t.step
					FROM {$this->table_name} twi
					JOIN tracking t ON twi.id = t.refid AND twi.step = t.step
					WHERE t.type = $this->step_type AND twi.id =  ".$id)->row();

        $data = array(
            "step" => $query->step - 1,
        );

        if ($data["step"] == 0){
            $data["approval_status"] = "Dibatalkan oleh MGR";
        } else if ($data["step"] == 1){
            $data["approval_status"] = "Dibatalkan oleh VP";
        } else {
            $data["approval_status"] = "Dibatalkan";
        }

        $this->db->where('id', $id);
        return $this->db->update($this->table_name, $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $result = $this->db->delete($this->table_name);
        return $result;
    }

    public function insertKomentar($data)
    {
        $data = array(
                "refid"            => $data['refid'],
                "type"            => $data['type'],
                "id_employee"    => $data['id_employee'],
                "komentar"       => $data['komentar'],
            );

        $result = $this->db->insert("history", $data);
        return $result;
    }
}
