<?php namespace App\Models\Operation;

use App\Models\_base\BaseTrackingModel;

class EnquiryModel extends BaseTrackingModel
{
    protected $table = "operation_enquiry";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["enquiry_date", "phone", "prospect_company",
        "website", "address", "prospect_name", "city", "email", "remark",
        "file_url", "step", "approval_status"];

    protected $step_type = 21;

}