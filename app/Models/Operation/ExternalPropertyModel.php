<?php namespace App\Models\Operation;

use App\Models\_base\BaseTrackingModel;

class ExternalPropertyModel extends BaseTrackingModel
{
    protected $table = "operation_external_property";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["date", "customer_name", "external_property_name", "external_property_type",
        "external_property_owner", "condition", "remark", "file_url", "step", "approval_status"];

    protected $step_type = 20;

}
