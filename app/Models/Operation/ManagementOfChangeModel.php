<?php namespace App\Models\Operation;

use App\Models\_base\BaseTrackingModel;

class ManagementOfChangeModel extends BaseTrackingModel
{
    protected $table = "operation_management_of_change";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["propose_change", "reason_for_change", "type_of_proposed_change",
        "possible_impact_of_change", "remark", "file_url", "step", "approval_status"];

    protected $step_type = 17;

}