<?php namespace App\Models\Operation;

use App\Models\_base\BaseTrackingModel;

class PreventiveMaintenanceModel extends BaseTrackingModel
{
    protected $table = "operation_preventive_maintenance";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["equipment", "frequency", "item", "activity",
        "responsibility", "remark", "file_url", "step", "approval_status"];

    protected $step_type = 16;

}
