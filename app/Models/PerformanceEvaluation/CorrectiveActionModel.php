<?php namespace App\Models\PerformanceEvaluation;

use App\Models\_base\BaseTrackingModel;

class CorrectiveActionModel extends BaseTrackingModel
{
    protected $step_type = 27;

    protected $table = "performance_evaluation_corrective_action";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = [
        "dept_location",
        "team_leader",
        "description",
        "team_member",
        "internal_external_team",
        "approval",
        "describe_issue",
        "describe_issue_date",
        "containment_plant",
        "containment_plant_date",
        "root_causes",
        "root_causes_date",
        "corrective_action",
        "corrective_action_date",
        "preventive_action_plan",
        "preventive_action_plan_date",
        "status",
        "status_date",
        "note",
        "file_url",
        "step",
        "approval_status"];

}