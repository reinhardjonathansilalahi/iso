<?php namespace App\Models\PerformanceEvaluation;

use App\Models\_base\BaseTrackingModel;

class CustomerSatisfactionModel extends BaseTrackingModel
{
    protected $table = "performance_evaluation_customer_satisfaction";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["nama", "perusahaan", "email", "no_telp",
        "komunikasi", "ketepatan_waktu", "kesesuaian_produk", "prosedur_pelayanan",
        "kompetensi", "respon_tambahan", "file_url", "step", "approval_status"];

    protected $step_type = 26;

}