<?php namespace App\Models\Planning;

use App\Models\_base\BaseModel;
use App\Models\_base\BaseTrackingModel;
use CodeIgniter\Model;

class ManagementOfChangeModel extends BaseTrackingModel
{

    protected $table = "planning_management_of_change";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["proposed_changes", "type_of_change", "reason_purpose_of_change",
        "possible_impact_of_change", "resource_availability", "approve_author",
        "file_url", "step", "approval_status"];

    protected $step_type = 29;

}

?>
