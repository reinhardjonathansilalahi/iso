<?php namespace App\Models\PolicyObjective;

use App\Models\_base\BaseTrackingModel;

class PolicyModel extends BaseTrackingModel
{
    protected $table = "policy";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = [
        "management_system_type", "policy_statement", "remarks",
        "approve_author", "file_url", "step", "approval_status"
    ];

    protected $step_type = 15;
}

?>
