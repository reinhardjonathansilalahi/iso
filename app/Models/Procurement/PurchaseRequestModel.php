<?php namespace App\Models\Procurement;

use App\Models\_base\BaseTrackingModel;

class PurchaseRequestModel extends BaseTrackingModel
{
    protected $table = "procurement_purchase_request";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["file_url", "date", "departemen", "item_code",
        "order_quantity", "uom", "description", "price_est", "required_date",
        "monthly_usage", "step", "approval_status"];

    protected $step_type = 4;

    // ada readItem
}
