<?php namespace App\Models\Procurement;

use App\Models\_base\BaseModel;

class VendorRegistrationModel extends BaseModel
{

    protected $table = "procurement_vendor_registration";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["company_type", "vendor_name", "directore_name",
        "address", "phone_number", "email", "npwp_number", "nib_number", "npwp_file", "nib_file"];

}

?>
