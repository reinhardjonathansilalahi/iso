<?php namespace App\Models\Smk3;

use App\Models\_base\BaseModel;

class ChecklistAuditEksternalModel extends BaseModel
{
    protected $table = "smk3_checklist_audit_eksternal";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["main", "submain", "subsub_main", "description", "description_expand",
        "doc_title", "rating", "status", "file_url"];

    public function getPercentages(){
        $countMainResult = $this->db->query("SELECT main, count(*) as jumlah FROM $this->table group by main ")->getResultObject();
        $indicatorResult = $this->db->query("SELECT main, rating FROM $this->table")->getResultObject();

        $percentage = array();
        $ratings = array();
        $mainCount = array();

        foreach ($countMainResult as $item){
            $mainCount[$item->main] = (int) $item->jumlah;
        }

        foreach ($countMainResult as $item){
            $ratings[$item->main] = 0;
        }

        foreach ($indicatorResult as $item){
            $ratings[$item->main] += $item->rating / 2.0;
        }

        foreach ($ratings as $key => $value){
            $percentage[$key] = $value / $mainCount[$key];
        }
//
//        foreach ($percentage as $key => $value){
//            if (is_integer($value)){
//                $percentage[$key] = 0;
//            }
//        }

        return $percentage;
    }

    public function getSubmainPercentages($main){
        $countSubMainResult = $this->db->query("SELECT main, sub_main, count(*) as jumlah FROM $this->table where main=$main group by main, sub_main")->getResultObject();
        $indicatorResult = $this->db->query("SELECT main, sub_main, rating FROM $this->table where main=$main")->getResultObject();

        $percentage = array();
        $ratings = array();
        $submainCount = array();

        foreach ($countSubMainResult as $item){
            $submainCount[$item->sub_main] = (int) $item->jumlah;
        }

        foreach ($countSubMainResult as $item){
            $ratings[$item->sub_main] = 0;
        }

        foreach ($indicatorResult as $item){
            $ratings[$item->sub_main] += $item->rating / 2.0;
        }

        foreach ($ratings as $key => $value){
            $percentage[$key] = $value / $submainCount[$key];
        }

        return $percentage;
    }


    public function get_indicators($where)
    {
//        return $this->db->query("SELECT * FROM $this->table WHERE main=$main and sub_main=\"$sub_main\"")->getResultObject();
        return $this->getWhere($where)->getResultObject();
    }
    public function insertItems($data, $table_name)
    {
        $this->db->table($table_name)->insert($data);
    }

    public function deleteItems($ref_id, $table_name)
    {
        return $this->db->table($table_name)->delete(array("ref_id" => $ref_id));
    }

}

?>
