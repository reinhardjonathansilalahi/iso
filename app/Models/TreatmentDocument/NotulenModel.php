<?php namespace App\Models\TreatmentDocument;

use App\Models\_base\BaseModel;

class NotulenModel extends BaseModel
{

    protected $table = "notulen";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["hari", "tanggal", "waktu",
        "tempat", "pic", "status", "konseptor", "topik",
        "uraian", "status_panjang"];

}

?>
