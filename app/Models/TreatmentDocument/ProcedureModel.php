<?php namespace App\Models\TreatmentDocument;

use App\Models\_base\BaseTrackingModel;

class ProcedureModel extends BaseTrackingModel
{
    protected $table = "treatment_procedure";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["header_title", "department", "revision", "date_issued",
        "scope", "goal", "definition", "general_information", "prosedure", "recording",
        "reference", "approval", "distribution", "file_url", "step", "approval_status"];

    protected $step_type = 2;

}

?>
