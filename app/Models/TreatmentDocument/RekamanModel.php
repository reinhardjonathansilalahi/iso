<?php namespace App\Models\TreatmentDocument;

use App\Models\_base\BaseTrackingModel;

class RekamanModel extends BaseTrackingModel
{
    protected $table = "treatment_rekaman";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = [
        "referensi", "header_title", "distribusi_1",
        "distribusi_2", "class_project", "date_time_location",
        "goal", "attendees", "description_activity", "result",
        "approval_1", "approval_2", "step", "approval_status"
    ];

    protected $step_type = 8;

}

?>
