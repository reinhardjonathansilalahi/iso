<?php

use App\FormEntities\DependentSelectChoice;
use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormData;
use App\FormEntities\FormRow;

?><!-- basic scripts -->

<!--[if !IE]> -->
<script src="<?php echo base_url("assets"); ?>/js/jquery-2.1.4.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url("assets"); ?>/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="<?php echo base_url("assets"); ?>/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="assets/js/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url("assets"); ?>/js/jquery-ui.custom.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/markdown.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/bootstrap-markdown.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/jquery.hotkeys.index.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/bootstrap-wysiwyg.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/bootbox.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/bootstrap-wysiwyg.min.js"></script>
<!-- ace scripts -->
<script src="<?php echo base_url("assets"); ?>/js/ace-elements.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/ace.min.js"></script>

<script src="<?php echo base_url("assets"); ?>/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/bootstrap-datepicker.min.js"></script>
<script>
    $("input[placeholder='yyyy-mm-dd']").datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        orientation: "bottom",
        todayHighlight: true,
    });
    $("input[placeholder='yyyy-mm-dd']").mask("9999-99-99");
    $("input[placeholder='yyyy-mm-dd']").css("width", "100%");
</script>

<!-- Bootstrap DataTables -->
<script src="https://unpkg.com/bootstrap-table@1.15.3/dist/bootstrap-table.min.js"></script>
<script src="https://unpkg.com/tableexport.jquery.plugin/tableExport.min.js"></script>
<script src="https://unpkg.com/tableexport.jquery.plugin/libs/jsPDF/jspdf.min.js"></script>
<script src="https://unpkg.com/tableexport.jquery.plugin/libs/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<!-- <script src="extensions/print/bootstrap-table-print.js"></script> -->
<script src="<?php echo base_url(); ?>assets/bootstrap-table/extensions/export/bootstrap-table-export.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">

    jQuery(function ($) {

        $('textarea[data-provide="markdown"]').each(function () {
            var $this = $(this);

            if ($this.data('markdown')) {
                $this.data('markdown').showEditor();
            } else $this.markdown()

            $this.parent().find('.btn').addClass('btn-white');
        })

        function parseMe(value) {
            return value.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        }

        var convertToHtmlTag = function (convertToHtmlTag) {
            var passValue = $("<span />", {html: convertToHtmlTag}).text();
            passValue = passValue.replace('&nbsp;', " ")
            return passValue
            //return document.createElement("span").innerText;
        };

        function showErrorAlert(reason, detail) {
            var msg = '';
            if (reason === 'unsupported-file-type') {
                msg = "Unsupported format " + detail;
            } else {
                //console.log("error uploading file", reason, detail);
            }
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
        }

        $(".wysiwyg-editor").on('keyup DOMSubtreeModified', function () {
            $(this).siblings("textarea").val(parseMe($(this).html()))
        });

        $(".wysiwyg-editor").each(function () {
            let placeholder = $(this).siblings("textarea").attr("placeholder");
            $(this).text(placeholder)
        });

        $(".wysiwyg-editor").ace_wysiwyg({
            toolbar: [
                // 'font',
                // null,
                // 'fontSize',
                // null,
                {name: 'bold', className: 'btn-info'},
                {name: 'italic', className: 'btn-info'},
                {name: 'strikethrough', className: 'btn-info'},
                {name: 'underline', className: 'btn-info'},
                null,
                {name: 'insertunorderedlist', className: 'btn-success'},
                {name: 'insertorderedlist', className: 'btn-success'},
                {name: 'outdent', className: 'btn-purple'},
                {name: 'indent', className: 'btn-purple'},
                null,
                {name: 'justifyleft', className: 'btn-primary'},
                {name: 'justifycenter', className: 'btn-primary'},
                {name: 'justifyright', className: 'btn-primary'},
                {name: 'justifyfull', className: 'btn-inverse'},
                null,
                {name: 'createLink', className: 'btn-pink'},
                {name: 'unlink', className: 'btn-pink'},
                null,
                // {name: 'insertImage', className: 'btn-success'},
                null,
                'foreColor',
                null,
                // {name: 'undo', className: 'btn-grey'},
                // {name: 'redo', className: 'btn-grey'}
            ],
            'wysiwyg': {
                fileUploadError: showErrorAlert
            }
        }).prev().addClass('wysiwyg-style2');

        //RESIZE IMAGE
        //Add Image Resize Functionality to Chrome and Safari
        //webkit browsers don't have image resize functionality when content is editable
        //so let's add something using jQuery UI resizable
        //another option would be opening a dialog for user to enter dimensions.
        if (typeof jQuery.ui !== 'undefined' && ace.vars['webkit']) {

            var lastResizableImg = null;

            function destroyResizable() {
                if (lastResizableImg == null) return;
                lastResizableImg.resizable("destroy");
                lastResizableImg.removeData('resizable');
                lastResizableImg = null;
            }

            var enableImageResize = function () {
                $('.wysiwyg-editor')
                    .on('mousedown', function (e) {
                        var target = $(e.target);
                        if (e.target instanceof HTMLImageElement) {
                            if (!target.data('resizable')) {
                                target.resizable({
                                    aspectRatio: e.target.width / e.target.height,
                                });
                                target.data('resizable', true);

                                if (lastResizableImg != null) {
                                    //disable previous resizable image
                                    lastResizableImg.resizable("destroy");
                                    lastResizableImg.removeData('resizable');
                                }
                                lastResizableImg = target;
                            }
                        }
                    })
                    .on('click', function (e) {
                        if (lastResizableImg != null && !(e.target instanceof HTMLImageElement)) {
                            destroyResizable();
                        }
                    })
                    .on('keydown', function () {
                        destroyResizable();
                    });
            }

            enableImageResize();

            /**
             //or we can load the jQuery UI dynamically only if needed
             if (typeof jQuery.ui !== 'undefined') enableImageResize();
             else {//load jQuery UI if not loaded
						//in Ace demo ./components will be replaced by correct components path
						$.getScript("assets/js/jquery-ui.custom.min.js", function(data, textStatus, jqxhr) {
							enableImageResize()
						});
					}
             */
        }


        <?php
        $addableFormRows = array();

        $formCols = array();
        foreach ($formArrayObject as $formRow) {
            if ($formRow instanceof FormRow) {
                if ($formRow->form_data instanceof FormData) {
                    array_push($addableFormRows, $formRow);
                } else {
                    foreach ($formRow->form_list as $formCol) {
                        if (is_array($formCol)) {
                            $array = $formCol;
                            foreach ($array as $formColItem) {
                                array_push($formCols, $formColItem);
                            }
                        } else {
                            array_push($formCols, $formCol);
                        }
                    }
                }
            }
        }
        ?>


        <?php
        echo <<<EOT
var encodedDependant = {};
EOT;
        ?>

        <?php
        foreach ($formCols as $formCol) {
            if ($formCol instanceof FormCol) {
                if (FormColType::FORM_SELECT) {
                    $selectDepependantChoice = $formCol->form_data;
                    if ($selectDepependantChoice instanceof DependentSelectChoice) {

                        $dependentToValueOf = $selectDepependantChoice->dependentToValueOf;

                        foreach ($formCols as $formCol2) {
                            if ($formCol2 instanceof FormCol) {
                                if ($formCol2->form_name == $dependentToValueOf) {

                                    // Just for references :
                                    // https://stackoverflow.com/questions/1801499/how-to-change-options-of-select-with-jquery
                                    // https://stackoverflow.com/questions/11179406/jquery-get-value-of-select-onchange

                                    // if u confused,
                                    // Please go and check form object for detail (ex Hiaro_environment_aspect_form.php)

                                    $encodedDependant = json_encode($selectDepependantChoice->dependantChoices);
                                    $form_name = $formCol2->form_name;

                                    echo <<<EOT
        
        encodedDependant["{$formCol->form_name}"] = JSON.parse('{$encodedDependant}')

        $('select[name={$form_name}]').on('change', function() {
       
            var newOptions = {}
            
            Object.entries(encodedDependant["{$formCol->form_name}"]).forEach(([key, value]) => {
                newOptions[key] = value;
            });
            
            var el = $("select[name={$formCol->form_name}]");
            el.empty(); // remove old options
            $.each(newOptions[this.value], function(key,value) {
              el.append($("<option></option>").attr("value", value).text(key));
            });
            
        });\n
EOT;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        ?>
    });
</script>

<?php if (count($addableFormRows) > 0): ?>
    <script type="text/javascript">

        function parseMe(value) {
            return value.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        }

        function showErrorAlert(reason, detail) {
            var msg = '';
            if (reason === 'unsupported-file-type') {
                msg = "Unsupported format " + detail;
            } else {
                //console.log("error uploading file", reason, detail);
            }
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
        }

        var convertToHtmlTag = function (convertToHtmlTag) {
            var passValue = $("<span />", {html: convertToHtmlTag}).text();
            passValue = passValue.replace('&nbsp;', " ")
            return passValue
            //return document.createElement("span").innerText;
        };

        function initWysiwygEditor() {
            $(".wysiwyg-editor[form-type=items]").on('keyup DOMSubtreeModified', function () {
                $(this).siblings("textarea").val(parseMe($(this).html()))
            });

            $(".wysiwyg-editor[form-type=items]").ace_wysiwyg({
                toolbar: [
                    // 'font',
                    // null,
                    // 'fontSize',
                    // null,
                    {name: 'bold', className: 'btn-info'},
                    {name: 'italic', className: 'btn-info'},
                    {name: 'strikethrough', className: 'btn-info'},
                    {name: 'underline', className: 'btn-info'},
                    null,
                    {name: 'insertunorderedlist', className: 'btn-success'},
                    {name: 'insertorderedlist', className: 'btn-success'},
                    {name: 'outdent', className: 'btn-purple'},
                    {name: 'indent', className: 'btn-purple'},
                    null,
                    {name: 'justifyleft', className: 'btn-primary'},
                    {name: 'justifycenter', className: 'btn-primary'},
                    {name: 'justifyright', className: 'btn-primary'},
                    {name: 'justifyfull', className: 'btn-inverse'},
                    null,
                    {name: 'createLink', className: 'btn-pink'},
                    {name: 'unlink', className: 'btn-pink'},
                    null,
                    // {name: 'insertImage', className: 'btn-success'},
                    null,
                    'foreColor',
                    null,
                    // {name: 'undo', className: 'btn-grey'},
                    // {name: 'redo', className: 'btn-grey'}
                ],
                'wysiwyg': {
                    fileUploadError: showErrorAlert
                }
            }).prev().addClass('wysiwyg-style2');
        }

        var backupValues = {}
        var id = 0;
        var numbers = [];

        function backup(tableName) {
            backupValues = {}
            backupValues[tableName] = []

            $("table[data-tablename=" + tableName + "] tbody").children().each(function (index, object) {
                backupValues[tableName][index] = []

                $(this).find("input[form-type=items], textarea[form-type=items]").each(function (id, obj) {
                    if ($(obj).prop("tagName").toLowerCase() === "input"){
                        backupValues[tableName][index][id] = $(obj).val()
                    } else if ($(obj).prop("tagName").toLowerCase() === "textarea"){
                       // backupValues[tableName][index][id] = tinyMCE.get($(obj).attr("id")).getContent()
                       backupValues[tableName][index][id] = $(obj).val()
                    }
                });
            })
        }

        function restore(tableName) {
            $("table[data-tablename=" + tableName + "] tbody").children().each(function (index, object) {
                if (backupValues[tableName][index] !== undefined) {
                    $(this).find("input[form-type=items], textarea[form-type=items]").each(function (id, obj) {
                        var value = backupValues[tableName][index][id];
                        if ($(obj).prop("tagName").toLowerCase() === "input"){
                            $(obj).val(value)
                        } else if ($(obj).prop("tagName").toLowerCase() === "textarea"){
                            // tinyMCE.get($(obj).attr("id")).setContent(value)
                            $(obj).siblings(".wysiwyg-editor[form-type=items]").html(convertToHtmlTag(value));
                        }
                    });
                }
            })
        }

        function refreshNumberOrder(tableName) {
            {
                let no = 1;
                $("p[id='field_no']").map(function () {
                    $(this).text(no);
                    no++;
                }).get();
            }

            {
                let id = 0;
                $("a[data-tablename=" + tableName).map(function () {
                    $(this).attr("onclick", "deleteRow(" + (id) + ", " + numbers[id] + ", \'" + tableName + "\')");
                    id++;
                }).get();
            }
        }

        function deleteRow(index, oldNumber, tableName) {
            backup(tableName);

            backupValues[tableName].splice(index, 1);
            numbers.splice(index, 1);

            $("table").bootstrapTable('remove', {
                field: 'no',
                values: ['<p id="field_no">' + oldNumber + '</p>']
            });

            initWysiwygEditor()

            refreshNumberOrder(tableName)
            restore(tableName)
            id--;
        }

        var rowObjects = {}

        function addRow(tableName) {

            backup(tableName);

            numbers.push(id + 1);
            let numberIndex = numbers.indexOf(id + 1);

            let row = rowObjects[tableName](id);

            row["no"] = '<p id="field_no">' + (id + 1) + '</p>';
            row["action"] = '<a onclick="deleteRow(' + numberIndex + ', ' + (id + 1) + ', \'' + tableName + '\')" data-action="delete_button" data-tablename="' + tableName + '" style="cursor: pointer; margin: 5px" title="Remove"><i class="fa fa-trash"></i></a>';

            $("table[data-tablename=" + tableName + "]").bootstrapTable('insertRow', {
                index: id,
                row: row
            });

            initWysiwygEditor();

            refreshNumberOrder(tableName)
            restore(tableName);
            id++;
        }

        <?php

        foreach ($addableFormRows as $formRow) {
            $object = "";
            $table_name = $formRow->form_data->table_name;

            echo <<<EOT
        
    rowObjects["{$table_name}"] = function(rowIndex){\n
        return {\n
EOT;

            foreach ($formRow->form_list as $formCol) {
                if ($formCol instanceof FormCol) {

                    if ($formCol->form_col_type == FormColType::FORM_INPUT) {
                        echo <<<EOT
            {$formCol->form_name}: '\\
                <div class="form-group" style="margin: 15px;">\\
                    <input class="form-control" form-type="items" name="{$table_name}['+ rowIndex +'][{$formCol->form_name}]" data-key="{$formCol->form_name}" type="text"/>\\
                </div>',\n
EOT;
                    } else if ($formCol->form_col_type == FormColType::FORM_TEXTAREA) {
                        echo <<<EOT
            {$formCol->form_name}: '\\
                <div class="form-group" style="margin: 15px;">\\
                    <div class="wysiwyg-editor" form-type="items" name="{$table_name}['+ rowIndex +'][{$formCol->form_name}]"></div>\\
                    <textarea style="display:none" class="form-control" form-type="items" name="{$table_name}['+ rowIndex +'][{$formCol->form_name}]" data-key="{$formCol->form_name}"></textarea>\\
                </div>',\n
EOT;
                    }

                }
            }

            echo <<<EOT
        }
    }\n
EOT;


        }
        ?>
    </script>
<?php endif; ?>

<?php
if (isset($customScripts))
    foreach ($customScripts as $customScript) {
        echo $customScript;
    }
?>

<?= isset($customScript) ? $customScript : "" ?>