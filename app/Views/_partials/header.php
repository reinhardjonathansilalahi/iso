<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>ISO - AsiaGlobalServis</title>

    <meta name="description" content="overview &amp; stats"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

    <!-- bootstrap & fontawesome -->
    <link id="bs-css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link id="bsdp-css" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css"
          rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url("assets"); ?>/font-awesome/4.5.0/css/font-awesome.min.css"/>

    <!-- page specific plugin styles -->
    <link rel="stylesheet" href="<?php echo base_url("assets"); ?>/css/jquery-ui.custom.min.css"/>

    <!-- text fonts -->
    <link rel="stylesheet" href="<?php echo base_url("assets"); ?>/css/fonts.googleapis.com.css"/>

    <!-- ace styles -->
    <link rel="stylesheet" href="<?php echo base_url("assets"); ?>/css/ace.min.css" class="ace-main-stylesheet"
          id="main-ace-style"/>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="<?php echo base_url("assets"); ?>/css/ace-part2.min.css" class="ace-main-stylesheet"/>
    <![endif]-->
    <link rel="stylesheet" href="<?php echo base_url("assets"); ?>/css/ace-skins.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url("assets"); ?>/css/ace-rtl.min.css"/>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="<?php echo base_url("assets"); ?>/css/ace-ie.min.css"/>
    <![endif]-->

    <!-- inline styles related to this page -->
    <!-- ace settings handler -->
    <script src="<?php echo base_url("assets"); ?>/js/ace-extra.min.js"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
    <script src="<?php echo base_url("assets"); ?>/js/html5shiv.min.js"></script>
    <script src="<?php echo base_url("assets"); ?>/js/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.15.3/dist/bootstrap-table.min.css">
    <link rel="stylesheet" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/vendors/css/charts/c3.css">
    <link rel="stylesheet" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/vendors/css/charts/d3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css">
<!--    <link rel="stylesheet" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/css-rtl/plugins/charts/c3-chart.css">-->

    <style>
        .page-header h1 small {
            margin: 0 0px;
            font-size: 14px;
            font-weight: 400;
            color: #8089A0;
        }

        .fileUpload {
            position: relative;
            overflow: hidden;
            margin: 10px;
        }

        .fileUpload input.upload {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }

        table.bordered tr td {
            padding: 20px;
            width: 150px;
            height: 30px;
            color: #555;
            background-color: #FFF;
            box-shadow: none;
            border: 1px solid;
            border-color: #D8D8D8 !important;
        }


        .right-arrow {
            width: 10px;
            float: right;
            font-weight: 700;
            font-size: 20px
        }

        .x_panel, .x_title {
            margin-bottom: 10px
        }

        .x_panel {
            width: 100%;
            padding: 10px 17px;
            display: inline-block;
            background: #fff;
            border: 1px solid #E6E9ED;
            -webkit-column-break-inside: avoid;
            -moz-column-break-inside: avoid;
            column-break-inside: avoid;
            opacity: 1;
            transition: all .2s ease
        }

        .x_title {
            border-bottom: 2px solid #E6E9ED;
            padding: 1px 5px 6px
        }

        .x_title .filter {
            width: 40%;
            float: right
        }

        .x_content, table.tile td ul li a, table.tile_info {
            width: 100%
        }

        .x_title h2 {
            margin: 5px 0 6px;
            float: left;
            display: block
        }

        .x_title h2 small {
            margin-left: 10px
        }

        .x_title span {
            color: #BDBDBD
        }

        .x_content {
            padding: 0 5px 6px;
            float: left;
            clear: both;
            margin-top: 5px
        }

        .x_content h4 {
            font-size: 16px;
            font-weight: 500
        }

        legend {
            padding-bottom: 7px
        }

        .demo-placeholder {
            height: 280px
        }

        .profile_details:nth-child(3n) {
            clear: both
        }

        .profile_details .profile_view {
            display: inline-block;
            padding: 10px 0 0;
            background: #fff
        }

        .profile_details .profile_view .divider {
            border-top: 1px solid #e5e5e5;
            padding-top: 5px;
            margin-top: 5px
        }

        .profile_details .profile_view .ratings {
            margin-bottom: 0;
            text-align: left;
            font-size: 16px
        }

        .profile_details .profile_view .bottom {
            background: #F2F5F7;
            padding: 9px 0;
            border-top: 1px solid #E6E9ED
        }

        .profile_details .profile_view .left {
            margin-top: 20px
        }

        .profile_details .profile_view .left p {
            margin-bottom: 3px
        }

        .profile_details .profile_view .right {
            margin-top: 0;
            padding: 10px
        }

        .profile_details .profile_view .img-circle {
            border: 1px solid #E6E9ED;
            padding: 2px
        }

        .profile_details .profile_view h2 {
            margin: 5px 0
        }

        .profile_details .profile_view .brief {
            margin: 0;
            font-weight: 300
        }

        .table-responsive > table > tbody > tr > td {
            text-align: center;
            vertical-align: middle;
        }
    </style>

    <script src="https://cdn.tiny.cloud/1/tnow3o3blitquq67bvgt5yejcbl3ctjb1sgwmmhox043m8xt/tinymce/5/tinymce.min.js"
            referrerpolicy="origin"></script>
</head>