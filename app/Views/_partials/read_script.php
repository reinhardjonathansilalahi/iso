<!-- basic scripts -->
<!--[if !IE]> -->
<script src="<?php echo base_url("assets"); ?>/js/jquery-2.1.4.min.js"></script>
<!-- <![endif]-->
<!--[if IE]>
<script src="<?php echo base_url("assets"); ?>/js/jquery-1.11.3.min.js"></script>
<![endif]-->

<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url("assets"); ?>/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="<?php echo base_url("assets"); ?>/js/bootstrap.min.js"></script>
<!-- page specific plugin scripts -->
<!--[if lte IE 8]>
<script src="<?php echo base_url("assets"); ?>/js/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url("assets"); ?>/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/jquery.dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/buttons.print.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/buttons.colVis.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/dataTables.select.min.js"></script>

<!-- ace scripts -->
<script src="<?php echo base_url("assets"); ?>/js/ace-elements.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/ace.min.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
    jQuery(function ($) {

        var tdLength = $("#dynamic-table thead tr").children().length
        var objArray = []
        objArray[0] = {"bSortable": false}
        objArray[tdLength - 2] = {"bSortable": false}
        objArray[tdLength - 3] = {"bSortable": false}
        objArray[tdLength - 4] = {"bSortable": false}
        objArray[tdLength - 5] = {"bSortable": false}

        for (let i = 1; i <= tdLength - 6; i++) {
            objArray[i] = null
        }

        //initiate dataTables plugin
        var myTable = $('#dynamic-table').DataTable({
            "scrollX": true,
            "aoColumns": objArray,
            "aaSorting": [],
            "headerCallback": function( thead, data, start, end, display ) {
                $(thead).children().each(function (index) {
                    $(this).css('text-align', 'center')
                });
            },
            columnDefs: [
                {"width": 10, "targets": [tdLength - 2, tdLength - 3, tdLength - 4, tdLength - 5]},
                {
                    "targets": '_all',
                    "createdCell": function (td, cellData, rowData, row, col) {
                        $(td).css('padding', '10px 20px')
                        $(td).css('vertical-align', 'middle')
                        $(td).css('text-align', 'center')
                    },
                }
            ],
        });
        //.wrap("<div class='dataTables_borderWrap' />")
        // if you are applying horizontal scrolling (sScrollX)

        $.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';

        new $.fn.dataTable.Buttons(myTable, {
            buttons: [
                {
                    "extend": "colvis",
                    "text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
                    "className": "btn btn-white btn-primary btn-bold",
                    columns: ':not(:first):not(:last)'
                },
                {
                    "extend": "copy",
                    "text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
                    "className": "btn btn-white btn-primary btn-bold"
                },
                {
                    "extend": "csv",
                    "text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
                    "className": "btn btn-white btn-primary btn-bold"
                },
                {
                    "extend": "excel",
                    "text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
                    "className": "btn btn-white btn-primary btn-bold"
                },
                {
                    "extend": "pdf",
                    "text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
                    "className": "btn btn-white btn-primary btn-bold"
                },
                {
                    "extend": "print",
                    "text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
                    "className": "btn btn-white btn-primary btn-bold",
                    autoPrint: false,
                    message: 'This print was produced using the Print button for DataTables'
                }
            ]
        });
        myTable.buttons().container().appendTo($('.tableTools-container'));

        //style the message box
        var defaultCopyAction = myTable.button(1).action();
        myTable.button(1).action(function (e, dt, button, config) {
            defaultCopyAction(e, dt, button, config);
            $('.dt-button-info').addClass('gritter-item-wrapper gritter-info gritter-center white');
        });

        /***************/

        function editItem(itemId) {
            window.location = "edit/" + itemId;
        }

        function distItem(itemId) {
            window.location = "distribution/" + itemId;
        }

        function deleteItem(itemId) {
            $('#modal-default').on('show.bs.modal', function (e) {
                $('.btn-ok', this).data('recordId', itemId);
            });

            $('#modal-default').on('click', '.btn-ok', function (e) {

                var id = $(this).data('recordId');

                var formData = {
                    'id': id
                };

                $.ajax({
                    type: 'POST',
                    url: '<?=base_url("internal_audit/non_conformance/process_delete")?>',
                    data: formData,     // data object
                    dataType: 'json',       // what type of data do we expect back from the server
                    encode: true,
                    error: function (request, status, error) {
                        alert(request.responseText);
                    }
                })

                    // using the done promise callback
                    .done(function (data) {

                        // here we will handle errors and validation messages
                        if (!data.success) {

                            Toast.fire({
                                type: 'error',
                                title: data.message
                            });

                        } else {

                            // ALL GOOD! just show the success message!
                            Toast.fire({
                                type: 'success',
                                title: data.message
                            });

                            setTimeout(function () {
                                location.reload();
                            }, 1000);

                        }
                    });
            });

            $('#modal-default').modal('show');
        }
    })

</script>