<?php

//$current_url = $this->uri->uri_string();
//$current_url = str_replace("/list", "", $current_url);
//$current_url = str_replace("/create", "", $current_url);
//$current_url = str_replace("/edit", "", $current_url);
?>

<div id="sidebar" class="sidebar responsive ace-save-state">

    <script type="text/javascript">
        try {
            ace.settings.loadState('sidebar')
        } catch (e) {
        }
    </script>

    <ul class="nav nav-list">
        <li class="<?php if (getUriSegment(1) == '') {
            echo 'active';
        } ?>">
            <a href="<?php echo base_url(); ?>">
                <i class="menu-icon fa fa-home"></i>
                <span class="menu-text"> Dashboard </span>
            </a>

            <b class="arrow"></b>
        </li>

        <li class="<?php if (getUriSegment(1) == 'calendar_of_event') {
            echo 'active';
        } ?>">
            <a href="<?php echo base_url("calendar_of_event/list"); ?>">
                <i class="menu-icon fa fa-calendar"></i>
                <span class="menu-text"> Calendar of Event </span>
            </a>

            <b class="arrow"></b>
        </li>

        <!--        <li class="--><?php //if (getUriSegment(1) == 'business_process') {
        //            echo 'active';
        //        } ?><!--">-->
        <!--            <a href="--><?php //echo base_url("business_process"); ?><!--">-->
        <!--                <i class="menu-icon fa fa-list-alt"></i>-->
        <!--                <span class="menu-text"> Business Process </span>-->
        <!--            </a>-->
        <!---->
        <!--            <b class="arrow"></b>-->
        <!--        </li>-->


        <li class="<?= (getUriSegment(1) == "sustainable_palmoil") ? "open" : "" ?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-tasks"></i>
                <span class="menu-text">
                    Sustainable Palm Oil
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="<?= (getUriSegment(2) == "ispo") ? "active" : "" ?>">
                    <a href="<?php echo base_url("sustainable_palmoil/ispo/main/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        ISPO
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "rspo") ? "active" : "" ?>">
                    <a href="<?php echo base_url("sustainable_palmoil/rspo/main/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        RSPO
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="<?= (getUriSegment(1) == "smk3") ? "open" : "" ?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-tasks"></i>
                <span class="menu-text">
                    SMK3
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="<?= (getUriSegment(2) == "checklist_audit_eksternal") ? "active" : "" ?>">
                    <a href="<?php echo base_url("smk3/checklist_audit_eksternal/main/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Checklist Audit Eksternal
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="<?= getUriSegment(1) == "planning" ? "open" : "" ?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-tasks"></i>
                <span class="menu-text">
                    Planning
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="<?= (getUriSegment(2) == "hiaro_environment_aspect") ? "active" : "" ?>">
                    <a href="<?php echo base_url("planning/hiaro_environment_aspect/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        HIARO & Environment Aspect
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "objective_planning") ? "active" : "" ?>">
                    <a href="<?php echo base_url("planning/objective_planning/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Objective & Planning
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "objective_evaluation") ? "active" : "" ?>">
                    <a href="<?php echo base_url("planning/objective_evaluation/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Objective Evaluation
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "regulation_register") ? "active" : "" ?>">
                    <a href="<?php echo base_url("planning/regulation_register/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Regulation Register
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "regulation_evaluation") ? "active" : "" ?>">
                    <a href="<?php echo base_url("planning/regulation_evaluation/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Regulation Evaluation
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "management_of_change") ? "active" : "" ?>">
                    <a href="<?php echo base_url("planning/management_of_change/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Management of Change
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="<?= getUriSegment(1) == "treatment_document" ? "open" : "" ?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-tasks"></i>
                <span class="menu-text">
                    Treatment Document
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="<?= (getUriSegment(2) == "procedure" && getUriSegment(3) != "list_dist") ? "active" : "" ?>">
                    <a href="<?php echo base_url("treatment_document/procedure/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Procedure
                    </a>

                    <b class="arrow"></b>
                </li>

                <!--                <li class="-->
                <? //= (getUriSegment(2) == "procedure" && getUriSegment(3) == "list_dist") ? "active" : "" ?><!--">-->
                <!--                    <a href="--><?php //echo base_url("treatment_document/procedure/list_dist") ?><!--">-->
                <!--                        <i class="menu-icon fa fa-caret-right"></i>-->
                <!--                        Distribusi-->
                <!--                    </a>-->
                <!---->
                <!--                    <b class="arrow"></b>-->
                <!--                </li>-->

                <li class="<?= (getUriSegment(2) == "work_instruction") ? "active" : "" ?>">
                    <a href="<?php echo base_url("treatment_document/work_instruction/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Work Instruction
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "rekaman") ? "active" : "" ?>">
                    <a href="<?php echo base_url("treatment_document/rekaman/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Recordings
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "usulan_dokumen") ? "active" : "" ?>">
                    <a href="<?php echo base_url("treatment_document/usulan_dokumen/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Proposed Document
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="<?= (getUriSegment(2) == "notulen") ? "active" : "" ?>">
                    <a href="<?php echo base_url("treatment_document/notulen/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Meeting Records
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="<?= (getUriSegment(1) == "admin") ? "open" : "" ?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-user"></i>
                <span class="menu-text">
                    Admin
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="<?= (getUriSegment(2) == "site") ? "active" : "" ?>">
                    <a href="<?php echo base_url("admin/site/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Sites
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "management_system_type") ? "active" : "" ?>">
                    <a href="<?php echo base_url("admin/management_system_type/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Management System Type
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "logo") ? "active" : "" ?>">
                    <a href="<?php echo base_url("") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Logo
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="<?= (getUriSegment(1) == "context_organization") ? "open" : "" ?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-institution"></i>
                <span class="menu-text">
                    Context of Organization
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="<?= (getUriSegment(2) == "understanding_context_organization") ? "active" : "" ?>">
                    <a href="<?php echo base_url("context_organization/understanding_context_organization/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Understanding the Organization and its Context
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "business_process") ? "active" : "" ?>">
                    <a href="<?php echo base_url("context_organization/business_process/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Business Process
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "external_issue") ? "active" : "" ?>">
                    <a href="<?php echo base_url("context_organization/external_issue/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        External Issue
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "internal_issue") ? "active" : "" ?>">
                    <a href="<?php echo base_url("context_organization/internal_issue/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Internal Issue
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "interested_party") ? "active" : "" ?>">
                    <a href="<?php echo base_url("context_organization/interested_party/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Interested Party
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "scope") ? "active" : "" ?>">
                    <a href="<?php echo base_url("context_organization/scope/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Scope Management System
                    </a>

                    <b class="arrow"></b>
                </li>

            </ul>
        </li>
        <li class="<?= (getUriSegment(1) == "hr") ? "open" : "" ?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-male"></i>
                <span class="menu-text">
                    HR
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="<?= (getUriSegment(2) == "department") ? "active" : "" ?>">
                    <a href="<?php echo base_url("hr/department/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Department
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "designation") ? "active" : "" ?>">
                    <a href="<?php echo base_url("hr/designation/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Designation
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "employee") ? "active" : "" ?>">
                    <a href="<?php echo base_url("hr/employee/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Employee
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "ideal_competence") ? "active" : "" ?>">
                    <a href="<?php echo base_url("hr/ideal_competence/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Ideal Competency
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "competence_evaluation") ? "active" : "" ?>">
                    <a href="<?php echo base_url("hr/competence_evaluation/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Competency Evaluation
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "training_identification") ? "active" : "" ?>">
                    <a href="<?php echo base_url("hr/training_identification/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Training Identification
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="<?= (getUriSegment(2) == "training_attendance") ? "active" : "" ?>">
                    <a href="<?php echo base_url("hr/training_attendance/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Training Attandance
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "training_evaluation") ? "active" : "" ?>">
                    <a href="<?php echo base_url("hr/training_evaluation/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Training Evaluation
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="<?= (getUriSegment(1) == "documented_information") ? "open" : "" ?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-briefcase"></i>
                <span class="menu-text">
                    Documented Information
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="<?= (getUriSegment(2) == "documents") ? "active" : "" ?>">
                    <a href="<?php echo base_url("documented_information/documents/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Dokumen
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "external_document") ? "active" : "" ?>">
                    <a href="<?php echo base_url("documented_information/external_document/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        External Document
                    </a>

                    <b class="arrow"></b>
                </li>

                <!--                <li class="-->
                <? //= (getUriSegment(2) == "external_document" && getUriSegment(3) == "note") ? "active" : "" ?><!--">-->
                <!--                    <a href="--><?php //echo base_url("documented_information/external_document/note") ?><!--">-->
                <!--                        <i class="menu-icon fa fa-caret-right"></i>-->
                <!--                        Private Note-->
                <!--                    </a>-->
                <!---->
                <!--                    <b class="arrow"></b>-->
                <!--                </li>-->
            </ul>
        </li>

        <li class="<?= (getUriSegment(1) == "policy_objective") ? "open" : "" ?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-bullhorn"></i>
                <span class="menu-text">
                    Policy and Objective
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="<?= (getUriSegment(2) == "policy") ? "active" : "" ?>">
                    <a href="<?php echo base_url("policy_objective/policy/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Policy
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "objective") ? "active" : "" ?>">
                    <a href="<?php echo base_url("policy_objective/objective/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Objective
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="<?= (getUriSegment(1) == "risk_opportunity") ? "open" : "" ?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-shield"></i>
                <span class="menu-text">
                    Risk and Opportunity
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="<?= (getUriSegment(1) == "risk_opportunity" && in_array(getUriSegment(2), array("list", "create", "edit"))) ? "active" : "" ?>">
                    <a href="<?php echo base_url("risk_opportunity/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Risk and Opportunity
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "type") ? "active" : "" ?>">
                    <a href="<?php echo base_url("risk_opportunity/type/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Risk and Opportunity Type
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="<?= (getUriSegment(2) == "category") ? "active" : "" ?>">
                    <a href="<?php echo base_url("risk_opportunity/category/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Risk Category
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>


        <li class="<?= (getUriSegment(1) == "internal_audit") ? "open" : "" ?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-check-square-o"></i>
                <span class="menu-text">
                    Internal Audit
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="<?= (getUriSegment(2) == "non_conformance") ? "active" : "" ?>">
                    <a href="<?php echo base_url("internal_audit/non_conformance/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Non Conformance
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "audit_planning") ? "active" : "" ?>">
                    <a href="<?php echo base_url("internal_audit/audit_planning/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Audit Planning
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "internal_audit_report") ? "active" : "" ?>">
                    <a href="<?php echo base_url("internal_audit/internal_audit_report/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Internal Audit Report
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>


        <li class="<?= (getUriSegment(1) == "procurement") ? "open" : "" ?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-cart-plus"></i>
                <span class="menu-text">
                    Procurement
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="<?= (getUriSegment(2) == "purchase_request") ? "active" : "" ?>">
                    <a href="<?php echo base_url("procurement/purchase_request/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Purchase Request
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "purchase_order") ? "active" : "" ?>">
                    <a href="<?php echo base_url("procurement/purchase_order/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Purchase Order
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "vendor_registration") ? "active" : "" ?>">
                    <a href="<?php echo base_url("procurement/vendor_registration/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Vendor Registration
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "vendor_evaluation") ? "active" : "" ?>">
                    <a href="<?php echo base_url("procurement/vendor_evaluation/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Vendor Evaluation
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="<?= (getUriSegment(1) == "management_review") ? "open" : "" ?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-check-square-o"></i>
                <span class="menu-text">
                    Management Review
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="<?= (getUriSegment(2) == "management_review") ? "active" : "" ?>">
                    <a href="<?php echo base_url("management_review/management_review/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Create Management Review
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "management_review_result") ? "active" : "" ?>">
                    <a href="<?php echo base_url("management_review/management_review_result/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Management Review Result
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>


        <li class="<?= (getUriSegment(1) == "operation") ? "open" : "" ?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-rocket"></i>
                <span class="menu-text">
                    Operation
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="<?= (getUriSegment(2) == "enquiry") ? "active" : "" ?>">
                    <a href="<?php echo base_url("operation/enquiry/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Enquiry
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "equipment") ? "active" : "" ?>">
                    <a href="<?php echo base_url("operation/equipment/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Equipment
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "preventive_maintenance") ? "active" : "" ?>">
                    <a href="<?php echo base_url("operation/preventive_maintenance/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Preventive Maintenance
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "calibration_log") ? "active" : "" ?>">
                    <a href="<?php echo base_url("operation/calibration_log/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Calibration Log
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "external_property") ? "active" : "" ?>">
                    <a href="<?php echo base_url("operation/external_property/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        External Property
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "management_of_change") ? "active" : "" ?>">
                    <a href="<?php echo base_url("operation/management_of_change/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Management of Change
                    </a>

                    <b class="arrow"></b>
                </li>

            </ul>
        </li>


        <li class="<?= (getUriSegment(1) == "performance_evaluation") ? "open" : "" ?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-rocket"></i>
                <span class="menu-text">
                    Performance Evaluation
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="<?= (getUriSegment(2) == "customer_satisfaction") ? "active" : "" ?>">
                    <a href="<?php echo base_url("performance_evaluation/customer_satisfaction/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Customer Satisfaction
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?= (getUriSegment(2) == "corrective_action") ? "active" : "" ?>">
                    <a href="<?php echo base_url("performance_evaluation/corrective_action/list") ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Corrective Action Request
                    </a>

                    <b class="arrow"></b>
                </li>

            </ul>
        </li>


        <li class="<?php if (getUriSegment(1) == 'rule') {
            echo 'active';
        } ?>">
            <a href="<?php echo base_url("rule/list"); ?>">
                <i class="menu-icon fa fa-key"></i>
                <span class="menu-text"> Rule </span>
            </a>

            <b class="arrow"></b>
        </li>


    </ul><!-- /.nav-list -->

    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state"
           data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>
</div>
