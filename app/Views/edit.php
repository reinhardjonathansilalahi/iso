<!DOCTYPE html>
<html lang="en">

<?php echo view('_partials/header'); ?>

<body class="no-skin">

<?php echo view('_partials/navbar'); ?>

<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

    <?php echo view('_partials/sidebar'); ?>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Home</a>
                    </li>
                    <li class="active"><?= $module_title ?></li>
                </ul><!-- /.breadcrumb -->
            </div>

            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                        <i class="ace-icon fa fa-cog bigger-130"></i>
                    </div>

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-navbar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-sidebar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-breadcrumbs" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-add-container" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->

                <div class="page-header">
                    <h1>
                        <?= $submodule_title ?>
                    </h1>
                </div><!-- /.page-header -->


                <!-- PAGE CONTENT BEGINS -->
                <form class="form-horizontal container-fluid" role="form" action="<?php echo $controller_path_url . "/process_edit" ?>" method="POST"
                      enctype="multipart/form-data">

                    <input style="display:none" type="text" name="id" id="form-field-1"/>

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"> Dept/Location Name </label>

                                <div class="col-sm-9">
                                    <input name="dept_location" class="form-control" value="HRD" type="text" readonly/>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"> Performance Indicator </label>

                                <div class="col-sm-9">
                                    <input name="performance_indicator" class="form-control" type="text"/>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"> Process Name </label>
                                <div class="col-sm-9">
                                    <div class="wysiwyg-editor" name="process_name"></div>
                                    <textarea style="display:none" name="process_name"></textarea>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"> Monitoring Methodology </label>

                                <div class="col-sm-9">
                                    <input name="monitoring_methodology" class="form-control" type="text"/>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"> Process Type </label>

                                <div class="col-sm-9">
                                    <select name="process_type" class="form-control select2" style="width: 100%;">
                                        <option value="" selected="selected" class="">Please select...</option>
                                        <option value="Main Process">Main Process</option>
                                        <option value="Sub Process">Sub Process</option>
                                    </select>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"> Measurement </label>

                                <div class="col-sm-9">
                                    <input name="measurement" class="form-control" type="text"/>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"> Input Category </label>

                                <div class="col-sm-9">
                                    <select name="input_category" class="form-control select2" style="width: 100%;">
                                        <option value="" selected="selected" class="">Please select...</option>
                                        <option value="Internal">Internal</option>
                                        <option value="External">External</option>
                                    </select>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"> Resources </label>

                                <div class="col-sm-9">
                                    <input name="resources" class="form-control" type="text"/>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"> Input </label>

                                <div class="col-sm-9">
                                    <input name="input" class="form-control" type="text"/>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"> Process/Documented Information </label>

                                <div class="col-sm-9">
                                    <input name="process_documented_information" class="form-control" type="text"/>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"> Output Category </label>

                                <div class="col-sm-9">
                                    <select name="output_category" class="form-control select2" style="width: 100%;">
                                        <option value="" selected="selected" class="">Please select...</option>
                                        <option value="Internal">Internal</option>
                                        <option value="External">External</option>
                                    </select>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"> Person in Charge </label>

                                <div class="col-sm-9">
                                    <input name="person_in_charge" class="form-control" type="text"/>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"> Output </label>

                                <div class="col-sm-9">
                                    <input name="output" class="form-control" type="text"/>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6"></div>
                    </div>

                    <div class="page-header">
                        <h4 style="font-weight: bold"> Create Risk </h4>
                    </div><!-- /.page-header -->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label> <?= lang("risk_description") ?> </label>
                                <div class="wysiwyg-editor" name="risk_description"></div>
                                <textarea style="display:none" name="risk_description"></textarea>
                            </div>
                        </div>
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" > Risk Impact </label>

                                <div class="col-sm-9">
                                    <select name="risk_impact" class="form-control select2" style="width: 100%;">
                                        <option value="" selected="selected" class="">Please select...</option>
                                        <option value="Low">Low</option>
                                        <option value="Medium">Medium</option>
                                        <option value="High">High</option>
                                    </select>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" > Risk Possibility </label>

                                <div class="col-sm-9">
                                    <select name="risk_possibility" class="form-control select2" style="width: 100%;">
                                        <option value="" selected="selected" class="">Please select...</option>
                                        <option value="Low">Low</option>
                                        <option value="Medium">Medium</option>
                                        <option value="High">High</option>
                                    </select>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" > Risk Category </label>

                                <div class="col-sm-9">
                                    <select name="risk_category" class="form-control select2" style="width: 100%;">
                                        <option value="" selected="selected">Please select...</option>
                                        <?php
                                        foreach ($riskCategories as $riskCategory) {
                                            $val = $riskCategory->id;
                                            $select = $riskCategory->risk_category;
                                            echo "<option value='$val'>$select</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div>

                    <div class="row">
                        <div class="col">
                            <label><?= lang("app.supporting_files") ?></label>
                            <input name="file_upload" type="file" id="file_choose" class="custom-file-input">
                        </div>
                    </div>

                    <div class="row">
                        <a href="<?= $row->file_url ?>" target="_blank">
                            <label style="cursor: pointer;">
                                <?= $row->file_url ?>
                            </label>
                        </a>
                    </div>

                    <div class="row clearfix form-actions">
                        <div class="col-md-offset-3 col-md-12">
                            <button class="btn btn-info" name="btn_submit" value="edit" type="submit"/>
                            <i class="ace-icon fa fa-check bigger-110"></i>
                            Submit
                        </div>
                    </div>

                </form>
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->

    

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery-2.1.4.min.js"></script>
<!-- <![endif]-->
<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->

<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url("assets/assets"); ?>/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="assets/js/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery-ui.custom.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/markdown.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-markdown.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery.hotkeys.index.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-wysiwyg.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootbox.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-wysiwyg.min.js"></script>
<!-- ace scripts -->
<script src="<?php echo base_url("assets/assets"); ?>/js/ace-elements.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/ace.min.js"></script>

<script src="<?php echo base_url("assets/assets"); ?>/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-datepicker.min.js"></script>
<script>
    $("input[placeholder='yyyy-mm-dd']").datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        orientation: "bottom",
        todayHighlight: true,
    });
    $("input[placeholder='yyyy-mm-dd']").mask("9999-99-99");
    $("input[placeholder='yyyy-mm-dd']").css("width", "100%");
</script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
    jQuery(function ($) {

        $('textarea[data-provide="markdown"]').each(function () {
            var $this = $(this);

            if ($this.data('markdown')) {
                $this.data('markdown').showEditor();
            } else $this.markdown()

            $this.parent().find('.btn').addClass('btn-white');
        })

        function parseMe(value) {
            return value.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        }

        var convertToHtmlTag = function (convertToHtmlTag) {
            var passValue = $("<span />", {html: convertToHtmlTag}).text();
            passValue = passValue.replace('&nbsp;', " ")
            return passValue
            //return document.createElement("span").innerText;
        };

        function showErrorAlert(reason, detail) {
            var msg = '';
            if (reason === 'unsupported-file-type') {
                msg = "Unsupported format " + detail;
            } else {
                //console.log("error uploading file", reason, detail);
            }
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
        }

        $(".wysiwyg-editor").on('keyup DOMSubtreeModified', function () {
            $(this).siblings("textarea").val(parseMe($(this).html()))
        });

        $(".wysiwyg-editor").ace_wysiwyg({
            toolbar: [
                'font',
                null,
                'fontSize',
                null,
                {name: 'bold', className: 'btn-info'},
                {name: 'italic', className: 'btn-info'},
                {name: 'strikethrough', className: 'btn-info'},
                {name: 'underline', className: 'btn-info'},
                null,
                {name: 'insertunorderedlist', className: 'btn-success'},
                {name: 'insertorderedlist', className: 'btn-success'},
                {name: 'outdent', className: 'btn-purple'},
                {name: 'indent', className: 'btn-purple'},
                null,
                {name: 'justifyleft', className: 'btn-primary'},
                {name: 'justifycenter', className: 'btn-primary'},
                {name: 'justifyright', className: 'btn-primary'},
                {name: 'justifyfull', className: 'btn-inverse'},
                null,
                {name: 'createLink', className: 'btn-pink'},
                {name: 'unlink', className: 'btn-pink'},
                null,
                {name: 'insertImage', className: 'btn-success'},
                null,
                'foreColor',
                null,
                {name: 'undo', className: 'btn-grey'},
                {name: 'redo', className: 'btn-grey'}
            ],
            'wysiwyg': {
                fileUploadError: showErrorAlert
            }
        }).prev().addClass('wysiwyg-style2');

        //RESIZE IMAGE

        //Add Image Resize Functionality to Chrome and Safari
        //webkit browsers don't have image resize functionality when content is editable
        //so let's add something using jQuery UI resizable
        //another option would be opening a dialog for user to enter dimensions.
        if (typeof jQuery.ui !== 'undefined' && ace.vars['webkit']) {

            var lastResizableImg = null;

            function destroyResizable() {
                if (lastResizableImg == null) return;
                lastResizableImg.resizable("destroy");
                lastResizableImg.removeData('resizable');
                lastResizableImg = null;
            }

            var enableImageResize = function () {
                $('.wysiwyg-editor')
                    .on('mousedown', function (e) {
                        var target = $(e.target);
                        if (e.target instanceof HTMLImageElement) {
                            if (!target.data('resizable')) {
                                target.resizable({
                                    aspectRatio: e.target.width / e.target.height,
                                });
                                target.data('resizable', true);

                                if (lastResizableImg != null) {
                                    //disable previous resizable image
                                    lastResizableImg.resizable("destroy");
                                    lastResizableImg.removeData('resizable');
                                }
                                lastResizableImg = target;
                            }
                        }
                    })
                    .on('click', function (e) {
                        if (lastResizableImg != null && !(e.target instanceof HTMLImageElement)) {
                            destroyResizable();
                        }
                    })
                    .on('keydown', function () {
                        destroyResizable();
                    });
            }

            enableImageResize();

            /**
             //or we can load the jQuery UI dynamically only if needed
             if (typeof jQuery.ui !== 'undefined') enableImageResize();
             else {//load jQuery UI if not loaded
						//in Ace demo ./components will be replaced by correct components path
						$.getScript("assets/js/jquery-ui.custom.min.js", function(data, textStatus, jqxhr) {
							enableImageResize()
						});
					}
             */
        }

        $('input[name=id]').val(convertToHtmlTag('<?= filter_output($row->id) ?>'));
        $('input[name=dept_location]').val(convertToHtmlTag('<?= filter_output($row->dept_location) ?>'));
        $('input[name=performance_indicator]').val(convertToHtmlTag('<?= filter_output($row->performance_indicator) ?>'));
        $('input[name=monitoring_methodology]').val(convertToHtmlTag('<?= filter_output($row->monitoring_methodology) ?>'));
        $('input[name=measurement]').val(convertToHtmlTag('<?= filter_output($row->measurement) ?>'));

        $('input[name=resources]').val(convertToHtmlTag('<?= filter_output($row->resources) ?>'));
        $('input[name=input]').val(convertToHtmlTag('<?= filter_output($row->input) ?>'));
        $('input[name=process_documented_information]').val(convertToHtmlTag('<?= filter_output($row->process_documented_information) ?>'));
        $('input[name=person_in_charge]').val(convertToHtmlTag('<?= filter_output($row->person_in_charge) ?>'));
        $('input[name=output]').val(convertToHtmlTag('<?= filter_output($row->output) ?>'));

        $('select[name=input_category]').val(convertToHtmlTag('<?= filter_output($row->input_category) ?>'));
        $('select[name=output_category]').val(convertToHtmlTag('<?= filter_output($row->output_category) ?>'));
        $('select[name=process_type]').val(convertToHtmlTag('<?= filter_output($row->process_type) ?>'));

        $('select[name=risk_impact]').val(convertToHtmlTag('<?= filter_output($row->risk_impact) ?>'));
        $('select[name=risk_possibility]').val(convertToHtmlTag('<?= filter_output($row->risk_possibility) ?>'));
        $('select[name=risk_category]').val(convertToHtmlTag('<?= filter_output($row->risk_category) ?>'));

        $('div[name=risk_description]').html(convertToHtmlTag('<?= filter_output($row->risk_description) ?>'));
        $('div[name=process_name]').html(convertToHtmlTag('<?= filter_output($row->process_name) ?>'));
    });
</script>
</body>
</html>
