<!DOCTYPE html>
<html lang="en">

<?php echo view('_partials/header'); ?>

<body class="no-skin">

<?php echo view('_partials/navbar'); ?>

<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

    <?php echo view('_partials/sidebar'); ?>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Home</a>
                    </li>
                    <li class="active"><?= $module_title ?></li>
                </ul><!-- /.breadcrumb -->
            </div>

            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                        <i class="ace-icon fa fa-cog bigger-130"></i>
                    </div>

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-navbar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-sidebar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-breadcrumbs" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-add-container" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->

                <div class="page-header">
                    <h1>
                        <?= $submodule_title ?>
                        <small><i class="ace-icon fa fa-angle-double-right"></i> Add New </small>
                    </h1>
                </div><!-- /.page-header -->

                <!-- PAGE CONTENT BEGINS -->
                <form class="form-horizontal container-fluid" role="form" action="<?php echo $controller_path_url . "/process_add" ?>"
                      method="POST" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"
                                       > Training Topic </label>

                                <div class="col-sm-9">
                                    <input name="training_topic" class="form-control" type="text"/>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"
                                       > Proposed Trainer </label>

                                <div class="col-sm-9">
                                    <input name="proposed_trainer" class="form-control" type="text"/>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"
                                       > <?= lang("department") ?> </label>

                                <div class="col-sm-9">
                                    <select name="department" class="form-control select2" style="width: 100%;">
                                        <option value="" selected="selected">Please select...</option>
                                        <?php
                                        foreach ($departments as $department) {
                                            $val = $department->id;
                                            $select = $department->department_name;
                                            echo "<option value='$val'>$select</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"
                                       > Training Type </label>

                                <div class="col-sm-9">
                                    <select name="training_type" class="form-control select2" style="width: 100%;">
                                        <option value="" selected="selected">Please select...</option>
                                        <option value="Classroom">Classroom</option>
                                        <option value="Online">Online</option>
                                        <option value="On the Job">On the Job</option>
                                    </select>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"
                                       > Planned Date </label>

                                <div class="col-sm-9">
                                    <input name="planned_date" class="form-control" placeholder="yyyy-mm-dd"
                                           type="text">
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"
                                       > <?= lang("remarks") ?> </label>

                                <div class="col-sm-9">
                                    <textarea name="remarks" form="form_objective" class="form-control" rows="3"
                                              placeholder=""></textarea>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"
                                       > Actual Date </label>

                                <div class="col-sm-9">
                                    <input name="actual_date" class="form-control" placeholder="yyyy-mm-dd" type="text">
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"
                                       > Proposed Evaluation Date </label>

                                <div class="col-sm-9">
                                    <input name="proposed_evaluation_date" class="form-control" placeholder="yyyy-mm-dd" type="text">
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"
                                       > Actual Trainer </label>

                                <div class="col-sm-9">
                                    <input name="actual_trainer" class="form-control" type="text"/>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"
                                       > Proposed Evaluation Methodology </label>

                                <div class="col-sm-9">
                                    <input name="proposed_evaluation_methodology" class="form-control" type="text"/>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <h4 style="font-weight: bold; margin-top: 30px;"> Create Trainees </h4 style="">

                                <div id="toolbar" style="margin-left: 15px">
                                    <div class="form-inline" role="form">
                                        <div class="form-group">
                                            <a onClick="addRow(false)">
                                                <button type="button" class="btn btn-labeled btn-primary">Add new
                                                </button>
                                            </a>
                                        </div>
                                        <!-- <button id="ok" type="submit" class="btn btn-primary">OK</button> -->
                                    </div>
                                </div>

                                <table
                                        id="table1"
                                        data-pagination="true"
                                        data-toggle="table"
                                        data-toolbar="#toolbar"
                                        data-show-refresh="true"
                                        data-show-toggle="true">
                                    <!-- data-url="https://examples.wenzhixin.net.cn/examples/bootstrap_table/data" -->
                                    <thead>
                                    <tr>
                                        <th data-field="no" data-width="5" data-width-unit="%">No</th>
                                        <th data-field="action" data-width="5" data-width-unit="%">Action</th>
                                        <th data-field="attendee_name" data-width="25" data-width-unit="%">Attendee Name</th>
                                        <th data-field="attended" data-width="10" data-width-unit="%">Attendend</th>
                                        <th data-field="remarks" data-width="40" data-width-unit="%"><?= lang("remarks") ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <h4 style="font-weight: bold; margin-top: 30px;"> Create Evaluator </h4 style="">

                                <div id="toolbar" style="margin-left: 15px">
                                    <div class="form-inline" role="form">
                                        <div class="form-group">
                                            <a onClick="addRow2(false)">
                                                <button type="button" class="btn btn-labeled btn-primary">Add new
                                                </button>
                                            </a>
                                        </div>
                                        <!-- <button id="ok" type="submit" class="btn btn-primary">OK</button> -->
                                    </div>
                                </div>

                                <table
                                        id="table2"
                                        data-pagination="true"
                                        data-toggle="table"
                                        data-toolbar="#toolbar2"
                                        data-show-refresh="true"
                                        data-show-toggle="true">
                                    <!-- data-url="https://examples.wenzhixin.net.cn/examples/bootstrap_table/data" -->
                                    <thead>
                                    <tr>
                                        <th data-field="no" data-width="5" data-width-unit="%">No</th>
                                        <th data-field="action" data-width="5" data-width-unit="%">Action</th>
                                        <th data-field="evaluator_name" data-width="25" data-width-unit="%">Evaluator Name</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- /.row -->

                    <div class="row clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn btn-info" type="submit ">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Submit
                        </div>
                    </div>
                </form>
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->

    

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery-2.1.4.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url("assets/assets"); ?>/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="assets/js/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery-ui.custom.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/markdown.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-markdown.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery.hotkeys.index.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-wysiwyg.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootbox.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-wysiwyg.min.js"></script>
 <!-- ace scripts -->
<script src="<?php echo base_url("assets/assets"); ?>/js/ace-elements.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/ace.min.js"></script>

<script src="<?php echo base_url("assets/assets"); ?>/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-datepicker.min.js"></script>
<script>
    $("input[placeholder='yyyy-mm-dd']").datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        orientation: "bottom",
        todayHighlight: true,
    });
    $("input[placeholder='yyyy-mm-dd']").mask("9999-99-99");
    $("input[placeholder='yyyy-mm-dd']").css("width", "100%");
</script>

<!-- Bootstrap DataTables -->
<script src="https://unpkg.com/bootstrap-table@1.15.3/dist/bootstrap-table.min.js"></script>
<script src="https://unpkg.com/tableexport.jquery.plugin/tableExport.min.js"></script>
<script src="https://unpkg.com/tableexport.jquery.plugin/libs/jsPDF/jspdf.min.js"></script>
<script src="https://unpkg.com/tableexport.jquery.plugin/libs/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<!-- <script src="extensions/print/bootstrap-table-print.js"></script> -->
<script src="<?php echo base_url(); ?>assets/bootstrap-table/extensions/export/bootstrap-table-export.js"></script>

<script>
    var convertToHtmlTag = function(convertToHtmlTag){
        var passValue = $("<span />", { html: convertToHtmlTag }).text();
        passValue = passValue.replace('&nbsp;', " ")
        return passValue
        //return document.createElement("span").innerText;
    };
</script>
<script>
var attendeeNameValues, remarksValues, attendedValues;
attendeeNameValues = [];
attendedValues = [];
remarksValues = [];

function backup(){

    attendeeNameValues = $("input[name='attendee_name[]']").map(function(){
        return $(this).val();
    }).get();

    attendedValues = $("input[name='attended[]']").map(function(){
        // return $(this).val();
        return $(this).is(":checked");
    }).get();

    remarksValues = $("input[name='remarks[]']").map(function(){
        return $(this).val();
    }).get();
}

function restore(){
    var index = 0;
    $("input[name='attendee_name[]']").map(function(){
        if (index < attendeeNameValues.length){
            $(this).val(attendeeNameValues[index]);
        }
        index++;
    }).get();

    var index = 0;
    $("input[name='attended[]']").map(function(){
        if (index < attendedValues.length){
            $(this).prop("checked", attendedValues[index]);
        }
        index++;
    }).get();

    var index = 0;
    $("input[name='remarks[]']").map(function(){
        if (index < remarksValues.length){
            $(this).val(remarksValues[index]);
        }
        index++;
    }).get();
}

var id = 0;
var numbers = [];

function refreshNumberOrder(){
    var no = 1;
    $("[id='field_no']").map(function(){
        $(this).text(no);
        no++;
    }).get();

    var no = 1;
    $("[id='delete_button']").map(function(){
        $(this).attr("onClick", "deleteRow("+(no-1)+", "+numbers[no-1]+")");
        no++;
    }).get();
}

function deleteRow(index, oldNumber){
    backup();

    attendeeNameValues.splice(index, 1);
    attendedValues.splice(index, 1);
    remarksValues.splice(index, 1);
    numbers.splice(index, 1);

    $("#table1").bootstrapTable('remove', {
        field: 'no',
        values: ['<p id="field_no">' + oldNumber +'</p>']
    });

    refreshNumberOrder();

    restore();
}

function addRow(init){
    if (!init){
        backup();
    }

    numbers.push(id+1);
    var numberIndex = numbers.indexOf(id+1);

    $("#table1").bootstrapTable('insertRow', {
        index : id,
        row   : {
            no : '<p id="field_no">' + (id+1) +'</p>',
            action : '\
            <a id="delete_button" onclick="deleteRow('+numberIndex+','+(id+1)+')" style="cursor: pointer; margin: 5px" title="Remove"><i class="fa fa-trash"></i></a>\
            ',
            attendee_name : '\
            <div id="attendee_name_group_'+(id+1)+'" class="form-group" style="margin: 15px;">\
                <input name="attendee_name[]" class="form-control" type="text"/>\
            </div>\
            ',
            attended : '\
            <div id="attended_group_'+(id+1)+'" class="form-group" style="margin: 15px;">\
                <input name="attended[]" class="form-control" type="checkbox"/>\
            </div>\
            ',
            remarks  : '\
            <div id="remarks_group'+(id+1)+'" class="form-group" style="margin: 15px;">\
                <input name="remarks[]" class="form-control"/>\
            </div>\
            '
        }
    });

    if (!init){
        restore();
    }

    id++;

    if (!init) {
        refreshNumberOrder();
    }
}

</script>


<script>
var evaluatorNameValues;
evaluatorNameValues = [];

function backup2(){

    evaluatorNameValues = $("input[name='evaluator_name[]']").map(function(){
        return $(this).val();
    }).get();
}

function restore2(){
    var index2 = 0;
    $("input[name='evaluator_name[]']").map(function(){
        if (index2 < evaluatorNameValues.length){
            $(this).val(evaluatorNameValues[index2]);
        }
        index2++;
    }).get();
}

var id2 = 0;
var numbers2 = [];

function refreshNumberOrder2(){
    var no = 1;
    $("[id='field_no2']").map(function(){
        $(this).text(no);
        no++;
    }).get();

    var no = 1;
    $("[id='delete_button']").map(function(){
        $(this).attr("onClick", "deleteRow("+(no-1)+", "+numbers2[no-1]+")");
        no++;
    }).get();
}

function deleteRow2(index2, oldNumber){
    backup2();

    evaluatorNameValues.splice(index2, 1);
    numbers2.splice(index2, 1);

    $("#table2").bootstrapTable('remove', {
        field: 'no',
        values: ['<p id="field_no2">' + oldNumber +'</p>']
    });

    refreshNumberOrder2();

    restore2();
}

function addRow2(init){
    if (!init){
        backup2();
    }

    numbers2.push(id2+1);
    var numberIndex = numbers2.indexOf(id2+1);

    $("#table2").bootstrapTable('insertRow', {
        index : id2,
        row   : {
            no : '<p id="field_no2">' + (id2+1) +'</p>',
            action : '\
            <a id="delete_button" onclick="deleteRow2('+numberIndex+','+(id2+1)+')" style="cursor: pointer; margin: 5px" title="Remove"><i class="fa fa-trash"></i></a>\
            ',
            evaluator_name : '\
            <div id="evaluator_name_group_'+(id2+1)+'" class="form-group" style="margin: 15px;">\
                <input name="evaluator_name[]" class="form-control" type="text"/>\
            </div>\
            '
        }
    });

    if (!init){
        restore2();
    }

    id2++;

    if (!init) {
        refreshNumberOrder2();
    }
}

$('form').submit(function (e) {
    e.preventDefault();

    $('.form-group').removeClass('has-error'); // remove the error class
    $('.help-block').remove(); // remove the error text
    $('.alert-success').remove();


    var datas = [];
    var datas2 = [];
    backup();
    backup2();

    // designation_id, attendeeNameValues, descriptionValues, remarksValues
    for (var i = 0; i < $("#table1").bootstrapTable('getData').length; i++){
        datas.push({
            no                          : i+1,
            number                      : numbers[i],
            attendee_name               : attendeeNameValues[i],
            attended                    : attendedValues[i],
            remarks                     : remarksValues[i]
        })
    }


    for (var i = 0; i < $("#table2").bootstrapTable('getData').length; i++){
        datas2.push({
            no                          : i+1,
            number                      : numbers2[i],
            evaluator_name              : evaluatorNameValues[i]
        })
    }

    var data = {
        remarks                         : $("textarea[name='remarks']").val(),
        actual_date                     : $("input[name='actual_date']").val(),
        actual_trainer                  : $("input[name='actual_trainer']").val(),
        proposed_evaluation_date        : $("input[name='proposed_evaluation_date']").val(),
        proposed_evaluation_methodology : $("input[name='proposed_evaluation_methodology']").val()
    };
    // $("select[name='designation'] option:selected").val()

    var formData = {
            'id'                 : <?= $id ?>,
            'datas'              : datas,
            'datas2'             : datas2,
            'data'               : data
    };

    // process the form
    $.ajax({
            type        : 'POST',
            url         : '<?=base_url("hr/training_attendance/process_edit")?>',
            data        : formData, // data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true,
            error       : function(data){
                alert("AJAX ERROR");
                alert(JSON.stringify(data));
            }
        })

        // using the done promise callback
        .done(function(data) {

            // alert(JSON.stringify(data));

            // log data to the console so we can see
            console.log(data);

            // here we will handle errors and validation messages
            if ( ! data.success) {

                alert('Please complete the form');

                for (var key in data.errors) {
                  eval(data.errors[key]);
                }


            } else {

                // ALL GOOD! just show the success message!
                // $('form[id=form_objective]').prepend('<div class="alert alert-success">' + data.message + '</div>');
                alert(data.message);

                // usually after form submission, you'll want to redirect
                // window.location = '/thank-you'; // redirect a user to another page

            }
        });
});
</script>

<script>
$(document).ready(function() {

    <?php
    echo "\n";
    foreach ($rows as $rowItem) {
        echo "\tattendeeNameValues.push('".filter_output($rowItem->attendee_name)."');\n";
        echo "\tattendedValues.push(".filter_output($rowItem->attended).");\n";
        echo "\tremarksValues.push('".filter_output($rowItem->remarks)."');\n";
        echo "\taddRow(true);\n";
    }
    ?>

    <?php
    echo "\n";
    foreach ($rows2 as $rowItem2) {
        echo "\tevaluatorNameValues.push('".filter_output($rowItem2->evaluator_name)."');\n";
        echo "\taddRow2(true);\n";
    }
    ?>

    restore();
    restore2();

    $("input[name='training_topic']").val(convertToHtmlTag('<?= $row->training_topic ?>'));
    $("input[name='training_topic']").attr('disabled', true);

    $("select[name='training_type']").val(convertToHtmlTag('<?= $row->training_type ?>'));
    $("select[name='training_type']").attr('disabled', true);

    $("input[name='proposed_trainer']").val(convertToHtmlTag('<?= $row->proposed_trainer ?>'));
    $("input[name='proposed_trainer']").attr('disabled', true);

    $("input[name='planned_date']").val(convertToHtmlTag('<?= $row->planned_date ?>'));
    $("input[name='planned_date']").attr('disabled', true);

    $("select[name='department']").val(convertToHtmlTag('<?= $row->department ?>'));
    $("select[name='department']").attr('disabled', true);

    $("textarea[name='remarks']").val(convertToHtmlTag('<?= filter_output($row->remarks) ?>'));

    $("input[name='actual_date']").val(convertToHtmlTag('<?= $row->actual_date ?>'));
    $("input[name='actual_trainer']").val(convertToHtmlTag('<?= $row->actual_trainer ?>'));
    $("input[name='proposed_evaluation_date']").val(convertToHtmlTag('<?= $row->proposed_evaluation_date ?>'));
    $("input[name='proposed_evaluation_methodology']").val(convertToHtmlTag('<?= $row->proposed_evaluation_methodology ?>'));
});
</script>
</body>
</html>
