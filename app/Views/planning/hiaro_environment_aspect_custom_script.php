<script>
    $("select[name=pre_consequences_benefit_initial_rating]").change(function() {
        let thisVal = $(this).val();
        let siblingVal =  $("select[name=pre_likelihood_initial_rating]").val();
        $("input[name=initial_risk_rating]").val(thisVal * siblingVal);
    });

    $("select[name=pre_likelihood_initial_rating]").change(function() {
        let thisVal = $(this).val();
        let siblingVal =  $("select[name=pre_consequences_benefit_initial_rating]").val();
        $("input[name=initial_risk_rating]").val(thisVal * siblingVal);
    });

    $("select[name=post_consequences_benefit_initial_rating]").change(function() {
        let thisVal = $(this).val();
        let siblingVal =  $("select[name=post_likelihood_initial_rating]").val();
        $("input[name=final_risk_rating]").val(thisVal * siblingVal);
    });

    $("select[name=post_likelihood_initial_rating]").change(function() {
        let thisVal = $(this).val();
        let siblingVal =  $("select[name=post_consequences_benefit_initial_rating]").val();
        $("input[name=final_risk_rating]").val(thisVal * siblingVal);
    });
</script>