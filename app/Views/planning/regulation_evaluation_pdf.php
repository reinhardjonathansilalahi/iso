<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Treatment Document</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <style type="text/css">
            table { page-break-inside:avoid }
            tr    { page-break-inside:avoid; page-break-after:auto }
            thead { display:table-header-group }
            tfoot { display:table-footer-group }
        </style>

    </head>
    <body>

        <table border="1"  cellspacing="0" cellpadding="2" style="width: 100%; margin-bottom: 30px; font-size: 10px;">
            <tr>
                <td style="width: 20%; height: 60px; text-align: center;" rowspan="4">
                    <img src="<?= $logo->url ?>" width="100%">
                </td>
                <td style="width: 50%; text-align: center; font-weight: bold;" rowspan="4">
                    Asia Global Servis
                </td>
                <td style="width: 30%; height: 20px;">
                    Doc :
                </td>
            </tr>
            <tr>
                <td style="height: 20px;">
                    Rev :
                </td>
            </tr>
            <tr>
                <td>
                    Referensi :
                </td>
            </tr>
            <tr>
                <td>
                    Klausul :
                </td>
            </tr>
            <tr>
                <td colspan="3" style="height: 30px; vertical-align: center">&nbsp;&nbsp;&nbsp;&nbsp;Title : </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    <?= lang("planning.regulation_title") ?>
                </td>
            </tr>
            <tr>
                <td style="height: 100px; padding-left: 20px;">
                    <?= clean_string(get_regulation_title_val($regulation_title)) ?>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    <?= lang("planning.clause") ?>
                </td>
            </tr>
            <tr>
                <td style="height: 100px; padding-left: 20px;">
                    <?= clean_string($clause) ?>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    <?= lang("planning.aspect") ?>
                </td>
            </tr>
            <tr>
                <td style="height: 100px; padding-left: 20px;">
                    <?= clean_string($aspect) ?>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    <?= lang("planning.fulfillment") ?>
                </td>
            </tr>
            <tr>
                <td style="height: 100px; padding-left: 20px;">
                    <?= clean_string($fulfillment) ?>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    <?= lang("planning.evidance") ?>
                </td>
            </tr>
            <tr>
                <td style="height: 100px; padding-left: 20px;">
                    <?= clean_string($evidance) ?>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    <?= lang("planning.remark") ?>
                </td>
            </tr>
            <tr>
                <td style="height: 100px; padding-left: 20px;">
                    <?= clean_string($remark) ?>
                </td>
            </tr>
        </table>

        <br /><br />

        <table border="1"  cellspacing="0" cellpadding="2" style="width: 100%;">
            <tr>
                <td class="p-3" style="font-size: 12px">
                    <div style="min-height: 100px;">
                        <div class="container-fluid">
                            <div class="row pl-3 pr-3">
                                <div class="col row">
                                    <div class="col" style="font-weight: bold">
                                        User
                                    </div>
                                    <div class="col" style="font-weight: bold">
                                        Apprv 1
                                    </div>
                                </div>
                                <div class="col row">
                                    <div align="center" class="col" style="margin-left: -3%; font-weight: bold;">
                                        Apprv 2
                                    </div>
                                </div>
                            </div>

                            <div style="height: 100px"></div>

                            <div class="row pl-3 pr-3">
                                <div class="col row">
                                    <div class="col ml-3 mr-3" style="border-bottom: 1px solid black;"></div>
                                    <div class="col ml-3 mr-3" style="border-bottom: 1px solid black;"></div>
                                </div>
                                <div class="col row">
                                    <div class="col ml-3 mr-3" style="border-bottom: 1px solid black;"></div>
                                    <div class="col ml-3 mr-3" style="border-bottom: 1px solid black;"></div>
                                </div>
                            </div>

                            <div class="row pl-3 pr-3">
                                <div class="col row">
                                    <div class="col">
                                        Tgl :
                                    </div>
                                    <div class="col">
                                        Tgl :
                                    </div>
                                </div>
                                <div class="col row">
                                    <div class="col">
                                        Tgl :
                                    </div>
                                    <div class="col">
                                        Tgl :
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>
