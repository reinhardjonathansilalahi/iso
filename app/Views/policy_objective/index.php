<?php
  echo view('_partials/header');
?>

  <style>
    .color-palette {
      height: 35px;
      line-height: 35px;
      text-align: center;
    }

    .color-palette-set {
      margin-bottom: 15px;
    }

    .color-palette span {
      display: none;
      font-size: 12px;
    }

    .color-palette:hover span {
      display: block;
    }

    .color-palette-box h4 {
      position: absolute;
      top: 100%;
      left: 25px;
      margin-top: -40px;
      color: rgba(255, 255, 255, 0.8);
      font-size: 12px;
      display: block;
      z-index: 7;
    }
  </style>

<?php
  echo view('_partials/navbar');
  echo view('_partials/sidebar');
?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h5><?= $title ?></h5>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Inline Charts</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
      <div class="container-fluid">


          <div class="row connectedSortable">


              <div id="col_summary" class="col-md-4">
                  <div class="card card-outline card-primary">
                      <div class="card-header">
                          <h3 class="card-title">
                              <i class="fas fa-text-width"></i>
                              Summary
                              <i style="float:right;" class="fas fa-bars"></i>
                          </h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body" style="min-height:350px;">

                          <div class="row">
                              <div class="col-lg">
                                <!-- small card -->
                                <div class="small-box bg-info">
                                  <div class="inner">
                                    <h3><?= $this->db->query("select * from policy")->num_rows() ?></h3>

                                    <p>Policies</p>
                                  </div>
                                  <div class="icon">
                                    <i class="fas ion-stats-bars"></i>
                                  </div>
                                  <a href="#" class="small-box-footer">
                                    More <i class="fas fa-arrow-circle-right"></i>
                                  </a>
                                </div>
                              </div>
                              <!-- ./col -->
                              <div class="col-lg">
                                <!-- small card -->
                                <div class="small-box bg-success">
                                  <div class="inner">
                                    <h3><?= $this->db->query("select * from objective")->num_rows() ?></h3>

                                    <p>Objectives</p>
                                  </div>
                                  <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                  </div>
                                  <a href="#" class="small-box-footer">
                                    More <i class="fas fa-arrow-circle-right"></i>
                                  </a>
                                </div>
                              </div>
                          </div>



                      </div>
                      <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
              </div>

              <div id="objective_department" class="col-md-4">
                  <div class="card card-outline card-primary">
                      <div class="card-header">
                          <h3 class="card-title">
                              <i class="fas fa-text-width"></i>
                              Objective (By Department)
                              <i style="float:right;" class="fas fa-bars"></i>
                          </h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body" style="min-height:350px;">

                          <div id="objective_bar_chart" style="height: 300px;"></div>

                      </div>
                      <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
              </div>
              <!-- ./col -->


              <div id="objective_management" class="col-md-4">
                  <div class="card card-outline card-primary">
                      <div class="card-header">
                          <h3 class="card-title">
                              <i class="fas fa-text-width"></i>
                              Objective (By <?= lang("management_system_type") ?>)
                              <i style="float:right;" class="fas fa-bars"></i>
                          </h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body" style="min-height:350px;">

                          <div id="objective_management_bar_chart" style="height: 300px;"></div>

                      </div>
                      <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
              </div>
              <!-- ./col -->


              <?php
                  $result = $this->db->query("select * from admin_management_system_type")->result();
                  $management_system_type_array = array();

                  foreach ($result as $item){
                      $management_system_type_array[$item->id] = $item->management_system_full_name;
                  }
              ?>


              <?php

                  foreach ($policies as $item){
                      $print = <<<EOT
                      <div id="col4" class="col-md-4">
                          <div class="card card-outline card-primary">
                              <div class="card-header">
                                  <h3 class="card-title">
                                      <i class="fas fa-text-width"></i>
                                      Policy
                                      <i style="float:right;" class="fas fa-bars"></i>
                                  </h3>
                              </div>
                              <!-- /.card-header -->
                              <div class="card-body" style="min-height:350px;">
                                <b>{$management_system_type_array[$item->management_system_type]}</b>
                                <hr />
                                $item->policy_statement
                              </div>
                              <!-- /.card-body -->
                          </div>
                          <!-- /.card -->
                      </div>
                      <!-- ./col -->
EOT;
                        echo $print;
                  }
              ?>



          </div>
          <!-- /.row -->


<?php

$html = <<<EOT
          <div class="row">


              <div class="col-md-6">
              <!-- DONUT CHART -->
                  <div class="card card-danger">
                      <div class="card-header">
                          <h3 class="card-title">Donut Chart Summary</h3>

                          <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-widget="remove"><i class="fas fa-times"></i></button>
                          </div>
                      </div>
                      <div class="card-body">

                          <canvas id="donutChart" style="height:230px"></canvas>

                      </div>
                      <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
              </div>



              <!-- /.col (LEFT) -->
              <div class="col-md-6">

                  <!-- STACKED BAR CHART -->
                  <div class="card card-success">
                      <div class="card-header">
                          <h3 class="card-title">Bar Chart Summary</h3>

                          <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-widget="remove"><i class="fas fa-times"></i></button>
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="chart">
                            <canvas id="stackedBarChart" style="height:230px"></canvas>
                          </div>
                      </div>
                      <!-- /.card-body -->
                  </div>
                  <!-- /.card -->

              </div>
              <!-- /.col (RIGHT) -->


          </div>
          <!-- /.row -->
EOT;

// echo $html;

?>


      </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->

</div>
<!-- /.content-wrapper -->



<?php
  echo view('_partials/footer');
?>



<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
<!-- FLOT CHARTS -->
<script src="<?php echo base_url(); ?>assets/plugins/flot/jquery.flot.js"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="<?php echo base_url(); ?>assets/plugins/flot-old/jquery.flot.resize.min.js"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script>
$(document).ready(function(){

    var order = $.cookie("order");

    if (order) {
        // alert(JSON.stringify(order));

        $(order.split(',')).each(function (i, id) {
            //appending the element with the ID given id should move each element to the end of the
            // list one after another, and at the end, the order should be restored.
            $("#"+id).appendTo($('.connectedSortable'));
        });
    }

    $('.connectedSortable').sortable({
        placeholder         : 'sort-highlight',
        connectWith         : '.connectedSortable',
        handle              : '.card-header, .nav-tabs',
        forcePlaceholderSize: true,
        zIndex              : 999999,
        update              : function(e,ui) {
            var order = $('.connectedSortable').sortable("toArray").toString();
            $.cookie("order", order);
            // alert(JSON.stringify(order));
        }
    });
    $('.connectedSortable .card-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');


});
</script>


<script src="<?php echo base_url(); ?>assets/plugins/chart.js/Chart.min.js"></script>

<script>
$(document).ready(function(){
    /*
     * BAR CHART
     * ---------
     */

<?php
    $result = $this->db->query("select * from hr_department")->result();
    $department_array = array();

    foreach ($result as $item){
        $department_array[$item->id] = $item->department_name;
    }
?>


<?php
    $result = $this->db->query("select * from admin_management_system_type")->result();
    $management_system_type_array = array();

    foreach ($result as $item){
        $management_system_type_array[$item->id] = $item->management_system_short_name;
    }
?>



    var objective_data  = {
        // data : [[1,10], [2,8], [3,4], [4,13], [5,17], [6,9]],
        data :
        <?php
            echo "[";
            $no = 0;
            foreach ($objectives as $item){
                ++$no;
                echo "[$no, {$item->count}], ";
            }
            echo "],";
         ?>
        bars: { show: true }
    }

    var objective_management_data  = {
        // data : [[1,10], [2,8], [3,4], [4,13], [5,17], [6,9]],
        data :
        <?php
            echo "[";
            $no = 0;
            foreach ($objectives_management as $item){
                ++$no;
                echo "[$no, {$item->count}], ";
            }
            echo "],";
         ?>
        bars: { show: true }
    }


    $.plot('#objective_bar_chart', [objective_data], {
        grid  : {
                borderWidth: 1,
                borderColor: '#f3f3f3',
                tickColor  : '#f3f3f3'
        },
        series: {
            bars: {
                show: true, barWidth: 0.5, align: 'center',
            },
        },
        colors: ['#3c8dbc'],
        xaxis : {
            // ticks: [[1,'January'], [2,'February'], [3,'March'], [4,'April'], [5,'May'], [6,'June']],
            ticks :
            <?php
                echo "[";
                $no = 0;
                foreach ($objectives as $item){
                    ++$no;
                    $key = $item->department;
                    $value = $department_array[$key];
                    echo "[$no, '{$value}'], ";
                }
                echo "],";
             ?>
        },
        xaxes: [
            { position: 'bottom', axisLabel: 'Departments', show: true, showTickLabels: "all", showMinorTicks: true, gridLines: true},
        ],
        yaxes: [
            { position: 'left', axisLabel: 'Count', show: true }
        ]
    })


    $.plot('#objective_management_bar_chart', [objective_data], {
        grid  : {
                borderWidth: 1,
                borderColor: '#f3f3f3',
                tickColor  : '#f3f3f3'
        },
        series: {
            bars: {
                show: true, barWidth: 0.5, align: 'center',
            },
        },
        colors: ['#3c8dbc'],
        xaxis : {
            // ticks: [[1,'January'], [2,'February'], [3,'March'], [4,'April'], [5,'May'], [6,'June']],
            ticks :
            <?php
                echo "[";
                $no = 0;
                foreach ($objectives_management as $item){
                    ++$no;
                    $key = $item->management_system_type;
                    $value = $management_system_type_array[$key];
                    echo "[$no, '{$value}'], ";
                }
                echo "],";
             ?>
        },
        xaxes: [
            { position: 'bottom', axisLabel: lang("management_system_type"), show: true, showTickLabels: "all", showMinorTicks: true, gridLines: true},
        ],
        yaxes: [
            { position: 'left', axisLabel: 'Count', show: true }
        ]
    })

});
</script>


</body>
</html>
