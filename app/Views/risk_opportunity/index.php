<?php
  echo view('_partials/header');
?>

  <style>
    .color-palette {
      height: 35px;
      line-height: 35px;
      text-align: center;
    }

    .color-palette-set {
      margin-bottom: 15px;
    }

    .color-palette span {
      display: none;
      font-size: 12px;
    }

    .color-palette:hover span {
      display: block;
    }

    .color-palette-box h4 {
      position: absolute;
      top: 100%;
      left: 25px;
      margin-top: -40px;
      color: rgba(255, 255, 255, 0.8);
      font-size: 12px;
      display: block;
      z-index: 7;
    }
  </style>

<?php
  echo view('_partials/navbar');
  echo view('_partials/sidebar');
?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h5><?= $title ?></h5>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Inline Charts</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
      <div class="container-fluid">


          <div class="row connectedSortable">


              <div id="col_summary" class="col-md-4">
                  <div class="card card-outline card-primary">
                      <div class="card-header">
                          <h3 class="card-title">
                              <i class="fas fa-text-width"></i>
                              Summary
                              <i style="float:right;" class="fas fa-bars"></i>
                          </h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body" style="min-height:350px;">

                          <div class="row">
                              <div class="col-lg">
                                <!-- small card -->
                                <div class="small-box bg-info">
                                  <div class="inner">
                                    <h3><?= $this->db->query("select * from policy")->num_rows() ?></h3>

                                    <p>Policies</p>
                                  </div>
                                  <div class="icon">
                                    <i class="fas ion-stats-bars"></i>
                                  </div>
                                  <a href="#" class="small-box-footer">
                                    More <i class="fas fa-arrow-circle-right"></i>
                                  </a>
                                </div>
                              </div>
                              <!-- ./col -->
                              <div class="col-lg">
                                <!-- small card -->
                                <div class="small-box bg-success">
                                  <div class="inner">
                                    <h3><?= $this->db->query("select * from objective")->num_rows() ?></h3>

                                    <p>Objectives</p>
                                  </div>
                                  <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                  </div>
                                  <a href="#" class="small-box-footer">
                                    More <i class="fas fa-arrow-circle-right"></i>
                                  </a>
                                </div>
                              </div>
                          </div>



                      </div>
                      <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
              </div>

              <div id="risk_type" class="col-md-4">
                  <div class="card card-outline card-primary">
                      <div class="card-header">
                          <h3 class="card-title">
                              <i class="fas fa-text-width"></i>
                              Risk (By Type)
                              <i style="float:right;" class="fas fa-bars"></i>
                          </h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body" style="min-height:350px;">

                          <div id="risk_type_bar_chart" style="height: 300px;"></div>

                      </div>
                      <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
              </div>
              <!-- ./col -->


              <div id="risk_category" class="col-md-4">
                  <div class="card card-outline card-primary">
                      <div class="card-header">
                          <h3 class="card-title">
                              <i class="fas fa-text-width"></i>
                              Risk (By Category)
                              <i style="float:right;" class="fas fa-bars"></i>
                          </h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body" style="min-height:350px;">

                          <div id="risk_category_bar_chart" style="height: 300px;"></div>

                      </div>
                      <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
              </div>
              <!-- ./col -->


              <div id="risk_donut" class="col-md-4">
                  <div class="card card-outline card-primary">
                      <div class="card-header">
                          <h3 class="card-title">
                              <i class="fas fa-text-width"></i>
                              Risk (By Impact)
                              <i style="float:right;" class="fas fa-bars"></i>
                          </h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body" style="min-height:350px;">

                          <div id="risk_donut_chart" style="height: 300px;"></div>

                      </div>
                      <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
              </div>
              <!-- ./col -->

          </div>
          <!-- /.row -->


      </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->

</div>
<!-- /.content-wrapper -->



<?php
  echo view('_partials/footer');
?>



<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
<!-- FLOT CHARTS -->
<script src="<?php echo base_url(); ?>assets/plugins/flot/jquery.flot.js"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="<?php echo base_url(); ?>assets/plugins/flot-old/jquery.flot.resize.min.js"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<script src="<?php echo base_url(); ?>assets/plugins/flot-old/jquery.flot.pie.min.js"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script>
$(document).ready(function(){

    var order = $.cookie("order");

    if (order) {
        // alert(JSON.stringify(order));

        $(order.split(',')).each(function (i, id) {
            //appending the element with the ID given id should move each element to the end of the
            // list one after another, and at the end, the order should be restored.
            $("#"+id).appendTo($('.connectedSortable'));
        });
    }

    $('.connectedSortable').sortable({
        placeholder         : 'sort-highlight',
        connectWith         : '.connectedSortable',
        handle              : '.card-header, .nav-tabs',
        forcePlaceholderSize: true,
        zIndex              : 999999,
        update              : function(e,ui) {
            var order = $('.connectedSortable').sortable("toArray").toString();
            $.cookie("order", order);
            // alert(JSON.stringify(order));
        }
    });
    $('.connectedSortable .card-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');


});
</script>


<script>
/*
 * Custom Label formatter
 * ----------------------
 */
function labelFormatter(label, series) {
  return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
    + label
    + '<br>'
    + Math.round(series.percent) + '%</div>'
}

var isDuplicate = function(a) {
    var counts = [];
    for(var i = 0; i <= a.length; i++) {
        if(counts[a[i]] === undefined) {
            counts[a[i]] = 1;
        } else {
            return true;
        }
    }
    return false;
}

var flatColor = function(h){
   var PHI = 0.618033988749895;
   var s, v, hue;
   if(h===undefined){
       hue = (Math.floor(Math.random()*(360 - 0 + 1)+0))/360;
       h = ( hue + ( hue / PHI )) % 360;
   }
   else h/=360;
   v = Math.floor(Math.random() * (100 - 20 + 1) + 20);
   s = (v-10)/100;
   v = v/100;

   var r, g, b, i, f, p, q, t;
   i = Math.floor(h * 6);
   f = h * 6 - i;
   p = v * (1 - s);
   q = v * (1 - f * s);
   t = v * (1 - (1 - f) * s);
   switch (i % 6) {
       case 0: r = v, g = t, b = p; break;
       case 1: r = q, g = v, b = p; break;
       case 2: r = p, g = v, b = t; break;
       case 3: r = p, g = q, b = v; break;
       case 4: r = t, g = p, b = v; break;
       case 5: r = v, g = p, b = q; break;
   }
       r = Math.round(r * 255);
       g = Math.round(g * 255);
       b = Math.round(b * 255);

   var finalColor = "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);

   return finalColor;

   // this.h = h;
   // this.s = s;
   // this.v = v;
   // this.r = r;
   // this.g = g;
   // this.b = b;
   // this.hex = finalColor;

}

$(document).ready(function(){
    /*
     * DONUT CHART
     * -----------
     */

    var colors = [
        flatColor(),
        flatColor(),
        flatColor(),
    ]

    while (isDuplicate(colors)){

        colors = [
           flatColor(),
           flatColor(),
           flatColor(),
       ]
    }

    <?php
        $low_count = $this->db->query("select * from risk_opportunity where risk_impact='Low'")->num_rows();
        $medium_count = $this->db->query("select * from risk_opportunity where risk_impact='Medium'")->num_rows();
        $high_count = $this->db->query("select * from risk_opportunity where risk_impact='High'")->num_rows();

        $total = $low_count + $medium_count + $high_count;
    ?>

    var donutData = [
      {
        label: 'Low',
        data : <?= $low_count/$total*100 ?>,
        color: colors[0]
      },
      {
        label: 'Medium',
        data : <?= $medium_count/$total*100 ?>,
        color: colors[1]
      },
      {
        label: 'High',
        data : <?= $high_count/$total*100 ?>,
        color: colors[2]
      }
    ]

    $.plot('#risk_donut_chart', donutData, {
      series: {
        pie: {
          show       : true,
          radius     : 1,
          innerRadius: 0.5,
          label      : {
            show     : true,
            radius   : 2 / 3,
            formatter: labelFormatter,
            threshold: 0.1
          }

        }
      },
      legend: {
        show: false
      }
    })
    /*
     * END DONUT CHART
     */
})
</script>


<script>
$(document).ready(function(){

    /*
     * BAR CHART
     * ---------
     */
<?php
    $result = $this->db->query("select * from risk_opportunity_type")->result();
    $risk_type_array = array();

    foreach ($result as $item){
        $risk_type_array[$item->id] = $item->risk_type;
    }
?>


<?php
    $result = $this->db->query("select * from risk_opportunity_category")->result();
    $risk_category_array = array();

    foreach ($result as $item){
        $risk_category_array[$item->id] = $item->risk_category;
    }
?>


    var risk_type_data  = {
        // data : [[1,10], [2,8], [3,4], [4,13], [5,17], [6,9]],
        data :
        <?php
            echo "[";
            $no = 0;
            foreach ($risk_types as $item){
                ++$no;
                echo "[$no, {$item->count}], ";
            }
            echo "],";
         ?>
        bars: { show: true }
    }

    var risk_category_data  = {
        // data : [[1,10], [2,8], [3,4], [4,13], [5,17], [6,9]],
        data :
        <?php
            echo "[";
            $no = 0;
            foreach ($risk_categories as $item){
                ++$no;
                echo "[$no, {$item->count}], ";
            }
            echo "],";
         ?>
        bars: { show: true }
    }


    $.plot('#risk_type_bar_chart', [risk_type_data], {
        grid  : {
                borderWidth: 1,
                borderColor: '#f3f3f3',
                tickColor  : '#f3f3f3'
        },
        series: {
            bars: {
                show: true, barWidth: 0.5, align: 'center',
            },
        },
        colors: ['#3c8dbc'],
        xaxis : {
            // ticks: [[1,'January'], [2,'February'], [3,'March'], [4,'April'], [5,'May'], [6,'June']],
            ticks :
            <?php
                echo "[";
                $no = 0;
                foreach ($risk_types as $item){
                    ++$no;
                    $key = $item->risk_opportunity_type;
                    $value = $risk_type_array[$key];
                    echo "[$no, '{$value}'], ";
                }
                echo "],";
             ?>
        },
        xaxes: [
            { position: 'bottom', axisLabel: 'Risk Type', show: true, showTickLabels: "all", showMinorTicks: true, gridLines: true},
        ],
        yaxes: [
            { position: 'left', axisLabel: 'Count', show: true }
        ]
    })


    $.plot('#risk_category_bar_chart', [risk_category_data], {
        grid  : {
                borderWidth: 1,
                borderColor: '#f3f3f3',
                tickColor  : '#f3f3f3'
        },
        series: {
            bars: {
                show: true, barWidth: 0.5, align: 'center',
            },
        },
        colors: ['#3c8dbc'],
        xaxis : {
            // ticks: [[1,'January'], [2,'February'], [3,'March'], [4,'April'], [5,'May'], [6,'June']],
            ticks :
            <?php
                echo "[";
                $no = 0;
                foreach ($risk_categories as $item){
                    ++$no;
                    $key = $item->risk_category;
                    $value = $risk_category_array[$key];
                    echo "[$no, '{$value}'], ";
                }
                echo "],";
             ?>
        },
        xaxes: [
            { position: 'bottom', axisLabel: 'Risk Categories', show: true, showTickLabels: "all", showMinorTicks: true, gridLines: true},
        ],
        yaxes: [
            { position: 'left', axisLabel: 'Count', show: true }
        ]
    })

});
</script>




</body>
</html>
